package com.app.mylineup.other;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.app.mylineup.custom_view.MyEditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static androidx.core.util.Preconditions.checkArgument;

public class Utility
{
    public static final String DATE_FORMAT_SHORT = "yyyy/MM/dd";
    public static final String DATE_TIME_FORMAT_SHORT = "yyyy-MM-dd HH:mm";
    public static final String DATE_TIME_SERVER_1 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String DATE_TIME_SERVER_2 = "yyyy/MM/dd HH/mm";
    public static final String DATE_FORMAT_COMPARE = "dd/MM/yyyy HH:mm:ss";
    public static final String DATE_FORMAT_ = "yyyy-MM-dd, EEE";
    public static final String datePattern = "yyyy-MM-dd";
    public static final String datePattern1 = "MM.dd.yyyy";
    public static final String datePattern_us = "MM/dd/yyyy";
    public static final String datePattern_af = "dd/MM/yyyy";

    public static final String wishlistDate = "MMMM dd,yyyy";
    public static final String dayDate = "dd";


    static int phoneNumberPreviousLength=0;
    static EditText editText;


    public static String getDayOfMonthSuffix(final int n) {
        checkArgument(n >= 1 && n <= 31, "illegal day of month: " + n);
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:  return "st";
            case 2:  return "nd";
            case 3:  return "rd";
            default: return "th";
        }
    }

    public static String getDateInFormat(String date)
    {
        //YYYY-MM-DD
        try {
            SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd");
            Date newDate=spf.parse(date);
            spf= new SimpleDateFormat("dd MMM yyyy");
            date = spf.format(newDate);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(newDate);
            SimpleDateFormat sdf = new SimpleDateFormat(Utility.dayDate);
            int day = Integer.parseInt(sdf.format(calendar.getTime()));
            sdf = new SimpleDateFormat("MMMM dd,yyyy");
            String p = sdf.format(calendar.getTime());
            int position = p.indexOf(',');
            date = p.substring(0,position)+Utility.getDayOfMonthSuffix(day)+p.substring(position,p.length());
            return date;
        }
        catch (Exception e)
        {return "";}

    }

    public static class PhoneNumberChecker implements TextWatcher
    {

        public PhoneNumberChecker(MyEditText etPhone)
        {
            editText = etPhone;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable s)
        {
            if(editText.getText().length() < phoneNumberPreviousLength)
            {
                phoneNumberPreviousLength = editText.getText().length();
                return;
            }

            String text = editText.getText().toString();
            phoneNumberPreviousLength = editText.getText().length();


            if(phoneNumberPreviousLength == 1)
            {
                editText.setText(new StringBuilder(text).insert(text.length()-1, "(").toString());
                editText.setSelection(editText.getText().length());
            }
            else if(phoneNumberPreviousLength == 5)
            {
                editText.setText(new StringBuilder(text).insert(text.length()-1, ") ").toString());
                editText.setSelection(editText.getText().length());
            }
            else if(phoneNumberPreviousLength == 10)
            {
                editText.setText(new StringBuilder(text).insert(text.length()-1, "-").toString());
                editText.setSelection(editText.getText().length());
            }
            phoneNumberPreviousLength = s.length();
        }
    }

}
