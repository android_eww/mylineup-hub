package com.app.mylineup.other;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.app.mylineup.pojo.loginData.LoginData;
import com.google.gson.Gson;

public class SharePref
{
    private static SharePref sharePref = new SharePref();
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    private SharePref() {} //prevent creating multiple instances by making the constructor private

    //The context passed into the getInstance should be Application level context.
    public static SharePref getInstance(Context context) {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(Constant.USER_PREF, Activity.MODE_PRIVATE);
            editor = sharedPreferences.edit();
        }
        return sharePref;
    }

    public void saveSession(String key,String placeObjStr) {
        editor.putString(key, placeObjStr);
        editor.commit();
    }

    public String getSession(String key) {
        return sharedPreferences.getString(key, "");
    }

    public LoginData getUserSession(String key){
        return new Gson().fromJson(sharedPreferences.getString(key, ""),LoginData.class);
    }

    public void removeSession(String key) {
        editor.remove(key);
        editor.commit();
    }

    public void clearAll() {
        editor.clear();
        editor.commit();
    }



}
