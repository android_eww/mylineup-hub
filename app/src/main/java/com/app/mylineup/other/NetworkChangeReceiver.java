package com.app.mylineup.other;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.app.mylineup.R;
import com.app.mylineup.activity.LoginActivity;
import com.app.mylineup.application.Application;
import com.app.mylineup.dialog.AlertDialogCommon;

public class NetworkChangeReceiver extends BroadcastReceiver
{
    AlertDialogCommon dialogCommon;

    @Override
    public void onReceive(Context context, Intent intent)
    {
        String status = NetworkUtil.getConnectivityStatusString(context);
        Log.e("TAG","status = "+status);

        if(dialogCommon != null)
        {dialogCommon.hideDialog();}
        if(status.equalsIgnoreCase("Not connected to Internet"))
        {
            dialogCommon = new AlertDialogCommon(Application.getInstance().getCurrentActivity());
            dialogCommon.showDialog(context.getString(R.string.app_name),context.getString(R.string.no_internet),context.getString(R.string.ok),"",true,false,null);
        }
    }
}
