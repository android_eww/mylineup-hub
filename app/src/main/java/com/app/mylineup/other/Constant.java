package com.app.mylineup.other;

public class Constant
{
    public static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
    public static final int REQUEST_STORAGE_WRITE_ACCESS_PERMISSION = 102;
    public static final int REQUEST_CAMERA_PERMISSION = 103;
    public static final int REQUEST_READ_CONTACT_PERMISSION = 107;

    public static final int OPEN_GALLERY = 106;
    public static final int CAMERA_RESULT = 104;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 105;
    public static final int REQUEST_SELECT_PICTURE_FOR_FRAGMENT = 0x02;

    public static String newgpsLatitude = "";
    public static String newgpsLongitude = "";

    public static String SPINNER = "dropdown";
    public static String  TEXTVIEW = "textbox";

    public static final String FROM_WISHLIST_DETAIL = "wishlist_detail";
    public static final String FROM_WISHLIST = "wishlist";
    public static final String FROM_USER_PROFILE = "user_profile";//another user
    public static final String FROM_MYPROFILE = "myprofile";
    public static final String FROM_GIFTFEED = "giftfeed";
    public static final String FROM_EDIT_LITTLE_ONE = "edit_little_one";
    public static final String FROM = "from";
    public static final String LITTLE_ONE_ID = "little_one_id";

    //Session
    public static final String USER_PREF = "user_pref";
    public static final String HEADER_KEY = "header_ket";
    public static final String HEADER_VALUE = "header_value";
    public static final String IS_LOGIN = "login";

    public static final String LOGIN_DATA = "login_data";
}
