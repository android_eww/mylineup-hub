package com.app.mylineup.application;

import android.app.Activity;
import android.content.Context;
import android.content.IntentFilter;

import com.app.mylineup.other.NetworkChangeReceiver;
import com.app.mylineup.other.SharePref;
import com.app.mylineup.web_services.APIClient;
import com.app.mylineup.web_services.Apis;
import com.github.tamir7.contacts.Contacts;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import retrofit2.Retrofit;

public class Application extends MultiDexApplication
{
    private static Application application;
    private Retrofit retrofit;
    private Apis apis;
    private Context context;
    private SharePref sharePref;
    private NetworkChangeReceiver networkChangeReceiver;
    private Activity activity;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
        context = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        sharePref = SharePref.getInstance(getApplicationContext());
        retrofit = APIClient.getClient(context,sharePref);
        apis = retrofit.create(Apis.class);
        application = this;
    }

    public static synchronized Application getInstance() {
        return application;
    }

    public SharePref getSharedPref()
    {
        return sharePref;
    }

    public Retrofit getRetrofitClient()
    {
        return retrofit;
    }

    public Apis getApis()
    {
        return apis;
    }

    public void registerReceiver()
    {
        networkChangeReceiver = new NetworkChangeReceiver();

        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        //filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        //filter.addAction("android.net.wifi.STATE_CHANGE");
        registerReceiver(networkChangeReceiver, filter);
    }

    public void setCurrentActivity(Activity activity)
    {
        this.activity = activity;
    }

    public Activity getCurrentActivity()
    {
        return activity;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        unregisterReceiver(networkChangeReceiver);
    }
}
