package com.app.mylineup.pojo.loginData;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginData {

@SerializedName("user")
@Expose
private User user;
@SerializedName("status")
@Expose
private Boolean status;
    @SerializedName("notification_setting")
    @Expose
    private NotificationSetting notificationSetting;

public User getUser() {
return user;
}

public void setUser(User user) {
this.user = user;
}

public Boolean getStatus() {
return status;
}

public void setStatus(Boolean status) {
this.status = status;
}

    public NotificationSetting getNotificationSetting() {
        return notificationSetting;
    }

    public void setNotificationSetting(NotificationSetting notificationSetting) {
        this.notificationSetting = notificationSetting;
    }

}


