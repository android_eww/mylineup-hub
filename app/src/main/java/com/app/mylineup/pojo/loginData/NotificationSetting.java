package com.app.mylineup.pojo.loginData;

import android.content.Intent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationSetting {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("new_connections")
    @Expose
    private Integer newConnections;
    @SerializedName("weekly_updates")
    @Expose
    private Integer weeklyUpdates;//
    @SerializedName("item_purchased")
    @Expose
    private Integer itemPurchased;
    @SerializedName("app_notification")
    @Expose
    private Integer appNotification;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getNewConnections() {
        return newConnections;
    }

    public void setNewConnections(Integer newConnections) {
        this.newConnections = newConnections;
    }

    public Integer getWeeklyUpdates() {
        return weeklyUpdates;
    }

    public void setWeeklyUpdates(Integer weeklyUpdates) {
        this.weeklyUpdates = weeklyUpdates;
    }

    public Integer getItemPurchased() {
        return itemPurchased;
    }

    public void setItemPurchased(Integer itemPurchased) {
        this.itemPurchased = itemPurchased;
    }

    public Integer getAppNotification() {
        return appNotification;
    }

    public void setAppNotification(Integer appNotification) {
        this.appNotification = appNotification;
    }

}