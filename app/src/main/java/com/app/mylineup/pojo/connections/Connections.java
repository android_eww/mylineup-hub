package com.app.mylineup.pojo.connections;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Connections {

@SerializedName("connections")
@Expose
private List<Connection> connections = null;
@SerializedName("status")
@Expose
private Boolean status;

public List<Connection> getConnections() {
return connections;
}

public void setConnections(List<Connection> connections) {
this.connections = connections;
}

public Boolean getStatus() {
return status;
}

public void setStatus(Boolean status) {
this.status = status;
}

}
