package com.app.mylineup.pojo;

public class WishlistImageBeen  {

    public int image;

    public WishlistImageBeen(int image)
    {
        this.image = image;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
