package com.app.mylineup.pojo;

import android.media.Image;

import java.util.ArrayList;
import java.util.List;

public class WishlistBeen {

    public String id, image, Title, status, itemNo;
    public List<WishlistImageBeen> wishlistImageBeens = new ArrayList<>();

    public WishlistBeen(String id, String image, String title, String status, String itemNo, List<WishlistImageBeen> wishlistImageBeens)
    {
        this.id = id;
        this.image = image;
        Title = title;
        this.status = status;
        this.itemNo = itemNo;
        this.wishlistImageBeens = wishlistImageBeens;
    }

    public List<WishlistImageBeen> getWishlistImageBeens() {
        return wishlistImageBeens;
    }

    public void setWishlistImageBeens(List<WishlistImageBeen> wishlistImageBeens) {
        this.wishlistImageBeens = wishlistImageBeens;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getItemNo() {
        return itemNo;
    }

    public void setItemNo(String itemNo) {
        this.itemNo = itemNo;
    }

    @Override
    public String toString() {
        return Title;
    }
}
