package com.app.mylineup.pojo.littleOnes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LittleOnes {

@SerializedName("little_one")
@Expose
private List<LittleOne> littleOne = null;
@SerializedName("status")
@Expose
private Boolean status;

public List<LittleOne> getLittleOne() {
return littleOne;
}

public void setLittleOne(List<LittleOne> littleOne) {
this.littleOne = littleOne;
}

public Boolean getStatus() {
return status;
}

public void setStatus(Boolean status) {
this.status = status;
}

}
