package com.app.mylineup.pojo;

public class OnlyString extends CommonPojo
{
    String title;

    public OnlyString(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return title;
    }
}
