package com.app.mylineup.web_services;

import android.content.Context;

import com.app.mylineup.other.Constant;
import com.app.mylineup.other.SharePref;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class NetworkInterceptor implements Interceptor {

    private Context myContext;
    private SharePref sharePref;
    public NetworkInterceptor(Context context, SharePref sharePref) {
        myContext = context;
        this.sharePref = sharePref;
    }

    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        Request originalRequest = chain.request();
        String key = sharePref.getSession(Constant.HEADER_KEY);
        String value = sharePref.getSession(Constant.HEADER_VALUE);
        Request newRequest;
        if(key.equalsIgnoreCase(""))
        {newRequest = originalRequest.newBuilder()
                .build();}
        else
        {newRequest = originalRequest.newBuilder()
                .header(key, value)
                .build();}

        return chain.proceed(newRequest);
    }
}
