package com.app.mylineup.web_services;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface Apis
{
    @GET("init/{version}/android/{id}")
    Call<Object> init(@Path("version") String version,@Path("id") String id);

    @FormUrlEncoded
    @POST("user_register")
    Call<Object> registerUser(@Field("full_name") String full_name,
                              @Field("phone") String phone,
                              @Field("email") String email,
                              @Field("password") String password,
                              @Field("device_token") String device_token,
                              @Field("device_type") String device_type,
                              @Field("latitude") String latitude,
                              @Field("longitude") String longitude,
                              @Field("device_name") String device_name);

    @FormUrlEncoded
    @POST("user_login")
    Call<Object> loginUser(@Field("email") String email,
                              @Field("password") String password,
                              @Field("device_token") String device_token,
                              @Field("device_type") String device_type,
                              @Field("latitude") String latitude,
                              @Field("longitude") String longitude);

    @FormUrlEncoded
    @POST("change_password")
    Call<Object> changePassword(@Field("current_password") String current_password,
                           @Field("new_password") String new_password,
                           @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("logout")
    Call<Object> logout(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("forgot_password")
    Call<Object> forgotPassword(@Field("email") String email);


    @FormUrlEncoded
    @POST("notification_setting_update")
    Call<Object> updateNotificationSetting(@Field("user_id") String user_id,
                                           @Field("setting_name") String setting_name, //Setting Name = new_connections/weekly_updates/item_purchased/app_notification
                                           @Field("status") String status);  //status (0/1)

    ///get and update myprofile
    //@FormUrlEncoded
    @Multipart
    @POST("myprofile")
    Call<Object> getUserProfile(@Part("user_id") RequestBody user_id,
                                @Part("full_name") RequestBody full_name,//optional
                                @Part("birthday_date") RequestBody birthday_date,//optional
                                @Part("anniversary_date") RequestBody anniversary_date,//optional
                                @Part("phone") RequestBody phone,//optional
                                @Part MultipartBody.Part profile_picture,//optional
                                @Part("zip_code") RequestBody zip_code,//optional
                                @Part("user_info") RequestBody user_info); //optional

    //"Date Format=YYYY-MM-DD (Ex. 2020-01-30)
    //phone format= (XXX) XXX-XXXX
    //profile_picture format: jpg|jpeg|png , Not more than 1 mb"

    @GET("little_one_data/{id}")
    Call<Object> getLittleOneData(@Path("id") String id);

    //add and edit little one
    @FormUrlEncoded
    @POST("little_one_add_edit")
    Call<Object> addLittleOne(@Field("user_id") String user_id,
                           @Field("first_name") String first_name,
                           @Field("last_name") String last_name,
                           @Field("birthday_date") String birthday_date,
                           @Field("little_one_id") String little_one_id,
                           @Field("user_info") String user_info);//Optional


    @FormUrlEncoded
    @POST("little_one_list")
    Call<Object> getLittleOne(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("little_one_delete")
    Call<Object> deleteLittleOne(@Field("little_one_id") String little_one_id);


    @FormUrlEncoded
    @POST("my_connections")
    Call<Object> myConnections(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("remove_profile_picture")
    Call<Object> removeprofilePic(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("little_one_list")
    Call<Object> littleOneList(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("wishlist_add_edit")
    Call<Object> addEditWishlist(@Field("user_id") String user_id,
                                 @Field("name") String name,
                                 @Field("wish_for_user_type") String wish_for_user_type,
                                 @Field("event_date") String event_date,
                                 @Field("type") String type,
                                 @Field("wishlist_id") String wishlist_id, //optional
                                 @Field("invites") String invites);//optional

    //Friend request send/ Accept/ Reject
    @FormUrlEncoded
    @POST("friend_status_update")
    Call<Object> friendRequest(@Field("user_id") String user_id,
                                 @Field("friend_id") String friend_id,
                                 @Field("status") String status);

    //"status: 0 / 1 / 2
    //0 - Send request, 1 - accept request, 2 -reject requst, 3 - Remove connection"

    //contact list
    @FormUrlEncoded
    @POST("user_list")
    Call<Object> findFriends(@Field("user_id") String user_id,
                               @Field("contacts") String contacts);
}