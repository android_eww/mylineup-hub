package com.app.mylineup.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.mylineup.R;
import com.app.mylineup.activity.CreateItemActivity;
import com.app.mylineup.activity.ItemDetailActivity;
import com.app.mylineup.custom_view.MyTextView;
import com.app.mylineup.databinding.RowWishlistBinding;
import com.app.mylineup.other.Constant;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class WishListAdapter extends RecyclerView.Adapter
{
    private Context context;
    private String type;

    public WishListAdapter(Context context,String type)
    {
        this.context = context;
        this.type = type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        RowWishlistBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_wishlist, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if(holder instanceof MyViewHolder)
        {
            MyViewHolder holder1 = (MyViewHolder) holder;

            if(type.equalsIgnoreCase(Constant.FROM_WISHLIST_DETAIL))
            {
                holder1.itemRowBinding.tvCategory.setText("Jordan Brand");
            }

        }
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    private class MyViewHolder extends RecyclerView.ViewHolder
    {
        public RowWishlistBinding itemRowBinding;

        public MyViewHolder(RowWishlistBinding binding) {
            super(binding.getRoot());
            itemRowBinding = binding;

            itemRowBinding.getRoot().setOnClickListener(v -> {
                Intent intent = new Intent(context, ItemDetailActivity.class);
                ((Activity)context).startActivity(intent);
                ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            });
        }

    }
}
