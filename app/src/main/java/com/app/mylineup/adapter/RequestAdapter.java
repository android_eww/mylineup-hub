package com.app.mylineup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.mylineup.R;
import com.app.mylineup.databinding.RowRequestBinding;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class RequestAdapter extends RecyclerView.Adapter {

    private Context context;
    private FriendRequstAction friendRequstAction;

    public RequestAdapter(Context context,FriendRequstAction friendRequstAction)
    {
        this.context = context;
        this.friendRequstAction = friendRequstAction;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        RowRequestBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_request,parent,false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if(holder instanceof  MyViewHolder)
        {
            MyViewHolder holder1 = (MyViewHolder) holder;
            holder1.binding.acceptRequest.setOnClickListener(v -> {
                if(friendRequstAction != null)
                    friendRequstAction.perFormActionOnRequest("1");
            });

            holder1.binding.declineRequest.setOnClickListener(v -> {
                if(friendRequstAction != null)
                    friendRequstAction.perFormActionOnRequest("2");
            });
        }

    }

    @Override
    public int getItemCount() {
        return 2;
    }

    private class MyViewHolder extends RecyclerView.ViewHolder
    {
        private RowRequestBinding binding;
        public MyViewHolder(RowRequestBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }


    public interface FriendRequstAction
    {
        void perFormActionOnRequest(String requestType);
    }
}
