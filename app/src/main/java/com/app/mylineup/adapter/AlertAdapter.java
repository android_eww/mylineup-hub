package com.app.mylineup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.app.mylineup.R;
import com.app.mylineup.custom_view.MyTextView;
import com.app.mylineup.pojo.CommonPojo;

import java.util.ArrayList;

public class AlertAdapter<T> extends ArrayAdapter<T>
{

    private LayoutInflater inflater;
    private ArrayList<T> list;

    public AlertAdapter(Context context, int resource, ArrayList<T> list)
    {
        super(context, resource);
        this.list = list;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public View getView(int position, View arg1, ViewGroup arg2)
    {
        View view = inflater.inflate(R.layout.item_listview_text, null);
        MyTextView myTextView = view.findViewById(R.id.tvName);
        myTextView.setText(list.get(position).toString());
        return view;
    }
}
