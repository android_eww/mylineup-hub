package com.app.mylineup.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.mylineup.R;
import com.app.mylineup.databinding.RowAboutItemBinding;
import com.app.mylineup.pojo.AboutMeBeen;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;


public class AboutAdapter extends RecyclerView.Adapter<AboutAdapter.MyViewHolder> {

    private String TAG = "AboutAdapter";
    private List<AboutMeBeen> aboutMeBeenList;
    private Context mContext;

    public AboutAdapter(Context context, List<AboutMeBeen> aboutMeBeenList)
    {
        this.mContext = context;
        this.aboutMeBeenList = aboutMeBeenList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        RowAboutItemBinding binding;

        public MyViewHolder(RowAboutItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        RowAboutItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_about_item,parent,false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        Log.e(TAG,"onBindViewHolder()");

        holder.binding.ivAbout.setImageResource(aboutMeBeenList.get(position).getImage());
        holder.binding.tvName.setText(aboutMeBeenList.get(position).getName());
        holder.binding.tvSize.setText(aboutMeBeenList.get(position).getSize());

        if(position % 2 != 0 || ((aboutMeBeenList.size()-1) == position  && (aboutMeBeenList.size() % 2 != 0) ))
        {holder.binding.view.setVisibility(View.GONE);}
        else
        {holder.binding.view.setVisibility(View.VISIBLE);}


    }

    @Override
    public int getItemCount()
    {
        return aboutMeBeenList.size();
    }
}