package com.app.mylineup.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.app.mylineup.R;
import com.app.mylineup.activity.ItemDetailActivity;
import com.app.mylineup.activity.UserProfileActivity;
import com.app.mylineup.activity.WishlistActivity;
import com.app.mylineup.databinding.RowWishlistItemBinding;
import com.app.mylineup.other.Constant;
import com.app.mylineup.pojo.WishlistBeen;
import com.app.mylineup.view.DepthPageTransformer;

import java.util.Calendar;
import java.util.List;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;


public class UserWishlistAdapter extends RecyclerView.Adapter<UserWishlistAdapter.MyViewHolder> {

    private String TAG = "PetAdapter";
    private List<WishlistBeen> wishlistBeenList;
    private Context mContext;

    private Long startClickTime;
    private long MAX_CLICK_DURATION = 100;

    boolean isMyAcc;

    public UserWishlistAdapter(Context context, List<WishlistBeen> wishlistBeenList, boolean isMyAcc)
    {
        this.mContext = context;
        this.wishlistBeenList = wishlistBeenList;
        this.isMyAcc = isMyAcc;
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        private RowWishlistItemBinding binding;

        public MyViewHolder(RowWishlistItemBinding binding)
        {
            super(binding.getRoot());
            this.binding = binding;

            binding.llOpenWishList.setOnClickListener(v -> {
                Intent intent = new Intent(mContext, WishlistActivity.class);
                if(!isMyAcc)
                {intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);}
                mContext.startActivity(intent);
                ((Activity)mContext).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            });

            binding.getRoot().setOnClickListener(v -> {
                Intent intent = new Intent(mContext, ItemDetailActivity.class);
                ((Activity)mContext).startActivity(intent);
                ((Activity)mContext).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            });

            binding.viewPager.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN: {
                            startClickTime = Calendar.getInstance().getTimeInMillis();
                            break;
                        }
                        case MotionEvent.ACTION_UP: {
                            long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                            if(clickDuration < MAX_CLICK_DURATION) {
                                //click event has occurred
                                Intent intent = new Intent(mContext, ItemDetailActivity.class);
                                ((Activity)mContext).startActivity(intent);
                                ((Activity)mContext).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                            }
                        }
                    }
                    return false;
                }
            });
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        RowWishlistItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),R.layout.row_wishlist_item, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        Log.e(TAG,"onBindViewHolder() images:- " + wishlistBeenList.get(position).getWishlistImageBeens());

        WishlistImageAdapter wishlistImageAdapter = new WishlistImageAdapter(mContext, wishlistBeenList.get(position).getWishlistImageBeens());
        holder.binding.viewPager.setAdapter(wishlistImageAdapter);
        holder.binding.circleIndicator.setViewPager(holder.binding.viewPager);
        holder.binding.viewPager.setPageTransformer(true, new DepthPageTransformer());
    }

    @Override
    public int getItemCount()
    {
        return wishlistBeenList.size();
    }
}