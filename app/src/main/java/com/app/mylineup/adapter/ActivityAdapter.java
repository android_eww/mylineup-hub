package com.app.mylineup.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.app.mylineup.R;
import com.app.mylineup.databinding.RowActivityItemBinding;
import com.app.mylineup.pojo.WishlistBeen;
import com.app.mylineup.view.DepthPageTransformer;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import me.relex.circleindicator.CircleIndicator;


public class ActivityAdapter extends RecyclerView.Adapter<ActivityAdapter.MyViewHolder> {

    private String TAG = "PetAdapter";
    private List<WishlistBeen> wishlistBeenList;
    private Context mContext;

    public ActivityAdapter(Context context, List<WishlistBeen> wishlistBeenList)
    {
        this.mContext = context;
        this.wishlistBeenList = wishlistBeenList;
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        private RowActivityItemBinding binding;

        public MyViewHolder(RowActivityItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        RowActivityItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),R.layout.row_activity_item, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        Log.e(TAG,"onBindViewHolder() images:- " + wishlistBeenList.get(position).getWishlistImageBeens());

        WishlistImageAdapter wishlistImageAdapter = new WishlistImageAdapter(mContext, wishlistBeenList.get(position).getWishlistImageBeens());
        holder.binding.viewPager.setAdapter(wishlistImageAdapter);
        holder.binding.circleIndicator.setViewPager(holder.binding.viewPager);
        holder.binding.viewPager.setPageTransformer(true, new DepthPageTransformer());
    }

    @Override
    public int getItemCount()
    {
        return wishlistBeenList.size();
    }
}