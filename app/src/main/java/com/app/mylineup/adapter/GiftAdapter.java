package com.app.mylineup.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.app.mylineup.R;
import com.app.mylineup.activity.AccountSettingActivity;
import com.app.mylineup.activity.UserProfileActivity;
import com.app.mylineup.databinding.RowGiftfeedsBinding;
import com.app.mylineup.other.Constant;
import com.app.mylineup.view.DepthPageTransformer;

import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class GiftAdapter extends RecyclerView.Adapter {

    private Context context;
    private Long startClickTime;
    private long MAX_CLICK_DURATION = 100;

    public GiftAdapter(Context context)
    {
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        RowGiftfeedsBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_giftfeeds,parent,false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if(holder instanceof MyViewHolder)
        {
            MyViewHolder holder1 = (MyViewHolder)holder;
            ImageAdapter imageAdapter = new ImageAdapter(context);
            holder1.binding.viewpager.setAdapter(imageAdapter);
            holder1.binding.circleIndicator.setViewPager(holder1.binding.viewpager);
            holder1.binding.viewpager.setPageTransformer(true, new DepthPageTransformer());
        }
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    private class MyViewHolder extends RecyclerView.ViewHolder
    {
        private RowGiftfeedsBinding binding;

        public MyViewHolder(RowGiftfeedsBinding binding)
        {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(v -> {
                Intent intent = new Intent(context, UserProfileActivity.class);
                intent.putExtra(Constant.FROM,Constant.FROM_USER_PROFILE);
                ((Activity)context).startActivity(intent);
                ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            });

            binding.viewpager.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN: {
                            startClickTime = Calendar.getInstance().getTimeInMillis();
                            break;
                        }
                        case MotionEvent.ACTION_UP: {
                            long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                            if(clickDuration < MAX_CLICK_DURATION) {
                                //click event has occurred
                                Intent intent = new Intent(context, UserProfileActivity.class);
                                intent.putExtra(Constant.FROM,Constant.FROM_USER_PROFILE);
                                ((Activity)context).startActivity(intent);
                                ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                            }
                        }
                    }
                    return false;
                }
            });
        }
    }
}
