package com.app.mylineup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mylineup.R;
import com.app.mylineup.databinding.ItemInviteBinding;
import com.app.mylineup.pojo.connections.Connection;
import com.squareup.picasso.Picasso;

import java.util.List;

public class InviteAdapter extends RecyclerView.Adapter
{
    private Context context;
    private List<Connection> list;

    public InviteAdapter(Context context, List<Connection> connectionList)
    {
        this.context = context;
        list = connectionList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        ItemInviteBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_invite,parent,false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if(holder instanceof MyViewHolder)
        {
            MyViewHolder holder1 = (MyViewHolder) holder;

            if(list.get(position).getProfilePicture() != null && !list.get(position).getProfilePicture().equalsIgnoreCase(""))
            {Picasso.get().load(list.get(position).getProfilePicture()).into(holder1.binding.ivAbout);}
            if(list.get(position).getFirstName() != null)
            {holder1.binding.tvName.setText(list.get(position).getFirstName()+" "+list.get(position).getLastName());}

            if(list.get(position).isSelected()){
            holder1.binding.ivSelected.setVisibility(View.VISIBLE);}
            else{ holder1.binding.ivSelected.setVisibility(View.GONE);}

            holder1.binding.materialRootView.setOnClickListener(v -> {
                if(list.get(position).isSelected())
                {list.get(position).setSelected(false);}
                else
                {list.get(position).setSelected(true);}
                notifyDataSetChanged();
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private class MyViewHolder extends RecyclerView.ViewHolder
    {
        private ItemInviteBinding binding;

        public MyViewHolder(ItemInviteBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
