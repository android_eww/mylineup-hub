package com.app.mylineup.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.app.mylineup.R;
import com.app.mylineup.activity.CreateLittleOneActivirt;
import com.app.mylineup.custom_view.MyTextView;
import com.app.mylineup.pojo.littleOnes.LittleOne;

import java.util.List;

public class CustomSpinner extends ArrayAdapter<LittleOne>
{
    private List<LittleOne> list;
    private Context context;
    private Spinner spinnerMadeFor;

    public CustomSpinner(Context context, int textViewResourceId, List<LittleOne> list, Spinner spinnerMadeFor) {
        super(context, textViewResourceId,list);
        this.list = list;
        this.context = context;
        this.spinnerMadeFor = spinnerMadeFor;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        View row;
        if(position == (list.size()-1))
        {
            row=inflater.inflate(R.layout.item_spinner_dropdown_last, parent, false);
        }
        else
        {
            row=inflater.inflate(R.layout.item_spinner_dropdown, parent, false);
            MyTextView tvName=row.findViewById(R.id.tvName);
            tvName.setText(list.get(position).toString());
        }

        return row;
    }

}
