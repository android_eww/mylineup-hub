package com.app.mylineup.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.mylineup.R;
import com.app.mylineup.activity.UserProfileActivity;
import com.app.mylineup.databinding.RowConnectionsBinding;
import com.app.mylineup.other.Constant;
import com.app.mylineup.pojo.connections.Connection;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class ConnectionAdapter extends RecyclerView.Adapter
{
    private Context context;
    private List<Connection> list;

    public ConnectionAdapter(Context context,List<Connection> connectionList)
    {
        this.context = context;
        this.list = connectionList;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        RowConnectionsBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_connections,parent,false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if(holder instanceof MyViewHolder)
        {
            MyViewHolder holder1 = (MyViewHolder) holder;
            if(list.get(position).getLastName() != null)
            {holder1.binding.tvName.setText(list.get(position).getFirstName()+" "+list.get(position).getLastName());}
            if(list.get(position).getProfilePicture() != null && !list.get(position).getProfilePicture().equalsIgnoreCase(""))
            {Picasso.get().load(list.get(position).getProfilePicture()).into(holder1.binding.ivConnection);}
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private class MyViewHolder extends RecyclerView.ViewHolder
    {
        private RowConnectionsBinding binding;

        public MyViewHolder(RowConnectionsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(v -> {
                Intent intent = new Intent(context, UserProfileActivity.class);
                intent.putExtra(Constant.FROM,Constant.FROM_USER_PROFILE);
                ((Activity)context).startActivity(intent);
                ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            });
        }
    }
}
