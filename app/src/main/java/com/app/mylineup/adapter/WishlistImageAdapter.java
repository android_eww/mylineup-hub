package com.app.mylineup.adapter;

import android.content.Context;
import android.nfc.Tag;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.mylineup.R;
import com.app.mylineup.pojo.WishlistImageBeen;


import java.util.List;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

public class WishlistImageAdapter extends PagerAdapter {

    public String TAG = "WishlistImageAdapter";
    private Context context;
    public List<WishlistImageBeen> wishlistImageBeens;

    public WishlistImageAdapter(Context context, List<WishlistImageBeen> wishlistImageBeens)
    {
        this.context = context;
        this.wishlistImageBeens = wishlistImageBeens;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position)
    {
        Log.e(TAG, "instantiateItem() Position:- " + position);
        Log.e(TAG, "instantiateItem() Image:- " + wishlistImageBeens.get(position).getImage());

        View view = LayoutInflater.from(context).inflate(R.layout.row_wishlist_image_item, null);
        ImageView imageView = view.findViewById(R.id.ivWishlist);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object view) {
        container.removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object)
    {
        return view == object;
    }

    @Override
    public int getCount() {
        return wishlistImageBeens.size();
    }
}
