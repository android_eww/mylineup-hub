package com.app.mylineup.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.mylineup.R;
import com.app.mylineup.activity.UserProfileActivity;
import com.app.mylineup.databinding.RowUpcomingOccasionBinding;
import com.app.mylineup.other.Constant;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class UpComingOccasionAdapter extends RecyclerView.Adapter
{
    private Context context;

    public UpComingOccasionAdapter(Context context)
    {
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        RowUpcomingOccasionBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_upcoming_occasion,parent,false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    private class MyViewHolder extends RecyclerView.ViewHolder
    {
        private RowUpcomingOccasionBinding binding;
        public MyViewHolder( RowUpcomingOccasionBinding binding)
        {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(view -> {
                Intent intent = new Intent(context, UserProfileActivity.class);
                intent.putExtra(Constant.FROM,Constant.FROM_USER_PROFILE);
                ((Activity)context).startActivity(intent);
                ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            });
        }
    }
}
