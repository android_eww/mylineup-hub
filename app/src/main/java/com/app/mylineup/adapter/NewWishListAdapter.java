package com.app.mylineup.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.mylineup.R;
import com.app.mylineup.activity.RegisterActivity;
import com.app.mylineup.activity.WishlistActivity;
import com.app.mylineup.databinding.RowNewWishlistBinding;
import com.app.mylineup.other.Constant;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class NewWishListAdapter extends RecyclerView.Adapter
{
    private Context context;

    public NewWishListAdapter(Context context)
    {
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        RowNewWishlistBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_new_wishlist,parent,false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if(holder instanceof MyViewHolder)
        {
            MyViewHolder holder1 = (MyViewHolder) holder;
        }
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    private class MyViewHolder extends RecyclerView.ViewHolder
    {
        RowNewWishlistBinding binding;

        public MyViewHolder(RowNewWishlistBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(v -> {
                Intent intent = new Intent(context, WishlistActivity.class);
                intent.putExtra(Constant.FROM, Constant.FROM_USER_PROFILE);
                context.startActivity(intent);
                ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            });
        }
    }
}
