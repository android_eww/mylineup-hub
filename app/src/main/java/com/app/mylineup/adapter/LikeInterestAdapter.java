package com.app.mylineup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mylineup.R;
import com.app.mylineup.databinding.RowLikeInterestBinding;
import com.app.mylineup.pojo.AboutMeBeen;

import java.util.List;

public class LikeInterestAdapter extends RecyclerView.Adapter
{
    private Context context;
    private List<AboutMeBeen> list;

    public LikeInterestAdapter(Context context, List<AboutMeBeen> interestList)
    {
        this.context = context;
        list = interestList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        RowLikeInterestBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_like_interest,parent,false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if(holder instanceof MyViewHolder)
        {
            MyViewHolder holder1 = (MyViewHolder) holder;
            holder1.binding.ivAbout.setImageResource(list.get(position).getImage());
            holder1.binding.tvName.setText(list.get(position).getName());
            holder1.binding.tvSize.setText(list.get(position).getSize());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    private class MyViewHolder extends RecyclerView.ViewHolder
    {
        private RowLikeInterestBinding binding;

        public MyViewHolder(RowLikeInterestBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
