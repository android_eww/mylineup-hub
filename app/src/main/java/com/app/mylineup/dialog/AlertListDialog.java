package com.app.mylineup.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.app.mylineup.R;
import com.app.mylineup.adapter.AlertAdapter;
import com.app.mylineup.pojo.AboutMeBeen;
import com.app.mylineup.pojo.CommonPojo;
import com.app.mylineup.pojo.WishlistBeen;

import java.util.ArrayList;
import java.util.List;

public class AlertListDialog extends AlertDialog.Builder
{
    private Context context;
    private AlertDialog dialog = null;
    private CallBackClickListener callBackClickListener;

    public AlertListDialog(Context context) {
        super(context);
        this.context = context;
    }

    public AlertListDialog(Context context, int themeResId) {
        super(context, themeResId);
        this.context = context;
    }

    public void showDialog(String title,boolean isNegativeDisplay,String negativeText,ArrayList<CommonPojo> list,CallBackClickListener callBackClickListener)
    {
        this.callBackClickListener = callBackClickListener;
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
        builderSingle.setTitle(title);

        View customView = LayoutInflater.from(context).inflate(R.layout.listview, null, false);
        ListView listView = customView.findViewById(R.id.listview);

        AlertAdapter mAdapter = new AlertAdapter<>(context, R.layout.item_listview_text, list);
        listView.setAdapter(mAdapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        builderSingle.setView(customView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("TAG","item = "+list.get(position).toString());
                dialog.dismiss();
                if(callBackClickListener != null)
                {
                    callBackClickListener.OnItemSelected(list.get(position),position);
                }
            }
        });

        if(isNegativeDisplay)
        {
            builderSingle.setNegativeButton(negativeText,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
        }

        dialog = builderSingle.create();
        dialog.show();
    }

    public interface CallBackClickListener
    {
        void OnItemSelected(CommonPojo commonPojo,int position);
    }
}
