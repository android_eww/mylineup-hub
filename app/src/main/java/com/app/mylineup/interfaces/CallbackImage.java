package com.app.mylineup.interfaces;

import android.graphics.Bitmap;
import android.net.Uri;

import com.app.mylineup.activity.ImageActivity;

public interface CallbackImage {
    void openImageChooser(ImageActivity.CallbackImageListener callbackImageListener,boolean isCameraOpen,boolean isRemoveImage);
    Uri getLocalBitmapUri(Bitmap bmp);
}
