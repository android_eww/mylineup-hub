package com.app.mylineup.interfaces;

import android.view.View;

public interface CallbackClickListner {
    void onClickListener(View view, int position);
}