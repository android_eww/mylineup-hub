package com.app.mylineup.interfaces;

import com.app.mylineup.pojo.myProfile.LittleOne;

public interface CallbackLittleOne
{
    void editLittleOne(String firstName,String lastName);
    void deleteLittleOne();
    void hideLittleOne();
    void addLittleOne(LittleOne littleOne);
}
