package com.app.mylineup.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.app.mylineup.R;
import com.app.mylineup.adapter.LittleOneAdapter;
import com.app.mylineup.custom_view.MyEditText;
import com.app.mylineup.custom_view.MyTextView;
import com.app.mylineup.databinding.ActivityProfileBinding;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.interfaces.CallbackLittleOne;
import com.app.mylineup.interfaces.LittleOneSingleton;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.Global;
import com.app.mylineup.other.Utility;
import com.app.mylineup.pojo.myProfile.LittleOne;
import com.app.mylineup.pojo.myProfile.MyProfile;
import com.app.mylineup.web_services.PrettyPrinter;
import com.squareup.picasso.Picasso;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fisk.chipcloud.Chip;
import fisk.chipcloud.ChipCloud;
import fisk.chipcloud.ChipCloudConfig;
import fisk.chipcloud.ChipListener;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfilerActivity extends ImageActivity
{
    private ActivityProfileBinding binding;

    private ArrayAdapter<String> adapter ;
    private ChipCloudConfig drawableWithCloseConfig;

    private List<List<String>> clothing_footwear_list,like_interest_list;
    private MyProfile userProfile;
    private ChipCloud storeChip,brandChip;
    private LittleOneAdapter littleOneAdapter;

    private String userBirthDate = "",userAnniversary="",imageFilePth = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile);
        init();
    }

    private void init()
    {
        setHeader(getString(R.string.edit_profile),null,null);

        binding.llCreateLittleOne.setOnClickListener(v -> {
            LittleOneSingleton.get(activity, new CallbackLittleOne() {
                @Override
                public void editLittleOne(String firstName, String lastName) {
                    Log.e("TAG","firstName = "+firstName);
                }
                @Override
                public void deleteLittleOne() {
                }
                @Override
                public void hideLittleOne() {
                }
                @Override
                public void addLittleOne(LittleOne littleOne) {
                    userProfile.getLittleOne().add(littleOne);
                    littleOneAdapter.notifyDataSetChanged();
                }
            });
            Intent intent = new Intent(activity, CreateLittleOneActivirt.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);

        });

        binding.tvSaveProfile.setOnClickListener(view -> {
            validateData();
        });

        binding.tvEditPhoto.setOnClickListener(v -> {
            callbackImage.openImageChooser((imageFilePath, imageUri, cameraPhotoOrientation) -> {
                if(imageFilePath != null)
                {this.imageUri = imageUri;
                    binding.ivProfile.setImageURI(imageUri);
                    this.imageFilePth = imageFilePath;}
                else
                {
                    this.imageUri = null;
                    binding.ivProfile.setImageResource(R.drawable.app_icon);
                    this.imageFilePth = null;
                    if(!loginData.getUser().getProfilePicture().contains("default.png"))
                    {callRemoveProfilePicApi();}
                }
            },false,false);
        });

        binding.tvAnniversaryDate.setOnClickListener(view -> {
            Global.openDatePicker(activity, false, false, new Global.CallbackDateSelect() {
                @Override
                public void OnDateSelect(Calendar myCalendar) {

                    SimpleDateFormat sdf = new SimpleDateFormat(Utility.datePattern);
                    userAnniversary = sdf.format(myCalendar.getTime());

                    sdf = new SimpleDateFormat(Utility.dayDate);
                    int day = Integer.parseInt(sdf.format(myCalendar.getTime()));
                    sdf = new SimpleDateFormat("MMMM dd, yyyy");
                    String p = sdf.format(myCalendar.getTime());
                    int position = p.indexOf(',');
                    String date = p.substring(0,position)+Utility.getDayOfMonthSuffix(day)+p.substring(position,p.length());
                    binding.tvAnniversaryDate.setText(date);
                }
            });
        });
        binding.tvBirthDate.setOnClickListener(view -> {
            Global.openDatePicker(activity, false, false, new Global.CallbackDateSelect() {
                @Override
                public void OnDateSelect(Calendar myCalendar) {

                    SimpleDateFormat sdf = new SimpleDateFormat(Utility.datePattern);
                    userBirthDate = sdf.format(myCalendar.getTime());

                    sdf = new SimpleDateFormat(Utility.dayDate);
                    int day = Integer.parseInt(sdf.format(myCalendar.getTime()));
                    sdf = new SimpleDateFormat("MMMM dd, yyyy");
                    String p = sdf.format(myCalendar.getTime());
                    int position = p.indexOf(',');
                    String date = p.substring(0,position)+Utility.getDayOfMonthSuffix(day)+p.substring(position,p.length());
                    binding.tvBirthDate.setText(date);
                }
            });
        });

        binding.etPhone.addTextChangedListener(new Utility.PhoneNumberChecker(binding.etPhone));

        callMyProfileApi();
    }

    private void callRemoveProfilePicApi()
    {
        loaderDialog.show();
        Call<Object> call = application.getApis().removeprofilePic(loginData.getUser().getId());
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG","response = "+x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(),dialogCommon,activity,application);
                try {
                    JSONObject jsonObject = new JSONObject(x);//{"status":true,"message":"Account activation mail is sent on your email address"}
                    if(jsonObject.has("status") && jsonObject.getBoolean("status"))
                    {
                        dialogCommon.showDialog(jsonObject.getString("message"));

                        loginData.getUser().setProfilePicture(jsonObject.getString("default"));
                        application.getSharedPref().saveSession(Constant.LOGIN_DATA,gson.toJson(loginData));
                    }
                    else
                    {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                }
                catch (Exception e)
                {}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    private void validateData()
    {
        if(!Global.isValidString(binding.etFirstName.getText().toString()))
        {dialogCommon.showDialog(getString(R.string.please_enter_full_name));return;}
        /*else if(!Global.isValidString(binding.etLastName.getText().toString()))
        {dialogCommon.showDialog(getString(R.string.please_enter_last_name));return;}*/
        else if(!Global.isValidString(binding.tvBirthDate.getText().toString()))
        {dialogCommon.showDialog(getString(R.string.please_select_date));return;}
        else if(!Global.isValidString(binding.etZipCode.getText().toString()))
        {dialogCommon.showDialog(getString(R.string.please_enter_zip_code));return;}
        else if(!Global.isValidString(binding.tvAnniversaryDate.getText().toString()))
        {dialogCommon.showDialog(getString(R.string.please_enter_anniversary_date));return;}
        else if(!Global.isValidString(binding.etPhone.getText().toString()))
        {dialogCommon.showDialog(getString(R.string.please_enter_phone_no));return;}

        String topBrand = "",favouriteStore="";
        for(int i = 0;i<storeChip.getAllChips().size();i++)
        { if(i == (storeChip.getAllChips().size() - 1)) {
            favouriteStore = favouriteStore+""+storeChip.getAllChips().get(i).getLabel(); }
            else {
            favouriteStore = favouriteStore+""+storeChip.getAllChips().get(i).getLabel()+",";}
        }

        for(int i = 0;i<brandChip.getAllChips().size();i++)
        { if(i == (brandChip.getAllChips().size() - 1)) {
            topBrand = topBrand+""+brandChip.getAllChips().get(i).getLabel();
            }else {
            topBrand = topBrand+""+brandChip.getAllChips().get(i).getLabel()+","; }
        }

        String fullname = binding.etFirstName.getText().toString().trim();
        //String lastName = binding.etLastName.getText().toString().trim();
        String zipCode = binding.etZipCode.getText().toString().trim();
        String phone = binding.etPhone.getText().toString().trim();


        try {
            JSONObject mainObject = new JSONObject();

            //add clothing and footwear
            JSONArray clothingFootwearArray = new JSONArray();
            for(int i = 0;i<binding.llClothingFootwear.getChildCount();i++)
            {
                JSONObject object = new JSONObject();
                String data = "";
                if(binding.llClothingFootwear.getChildAt(i) instanceof LinearLayout)
                {
                    LinearLayout linearLayout = (LinearLayout) binding.llClothingFootwear.getChildAt(i);
                    if(linearLayout.getChildAt(1) instanceof Spinner)
                    { Spinner spinner = (Spinner) linearLayout.getChildAt(1);
                        data = spinner.getSelectedItemPosition() > 0 ? userProfile.getClothingFootware().get(i).getData().get(spinner.getSelectedItemPosition()-1) : ""; }
                    else if(linearLayout.getChildAt(1) instanceof EditText)
                    { EditText editText = (MyEditText) linearLayout.getChildAt(1);
                        data = editText.getText().toString().trim(); }
                }
                object.put("field_id",userProfile.getClothingFootware().get(i).getFieldId());
                object.put("data",data);

                clothingFootwearArray.put(object);
            }

            mainObject.put("clothing_footware",clothingFootwearArray);

            //add like and interest array
            JSONArray likeInterestArray = new JSONArray();
            for(int i = 0;i<binding.llLikeInterest.getChildCount();i++)
            {
                JSONObject object = new JSONObject();
                String data = "";
                if(binding.llLikeInterest.getChildAt(i) instanceof LinearLayout)
                { LinearLayout linearLayout = (LinearLayout) binding.llLikeInterest.getChildAt(i);
                    if(linearLayout.getChildAt(1) instanceof Spinner)
                    { Spinner spinner = (Spinner) linearLayout.getChildAt(1);
                        data = spinner.getSelectedItemPosition() > 0 ? userProfile.getLikesInterest().get(i).getData().get(spinner.getSelectedItemPosition()-1) : ""; }
                    else if(linearLayout.getChildAt(1) instanceof EditText)
                    { EditText editText = (MyEditText) linearLayout.getChildAt(1);
                        data = editText.getText().toString().trim(); }
                }
                object.put("field_id",userProfile.getLikesInterest().get(i).getFieldId());
                object.put("data",data);

                likeInterestArray.put(object);
            }

            mainObject.put("likes_interest",likeInterestArray);

            //add top brand anf favourite store
            mainObject.put("favourite_store",favouriteStore);
            mainObject.put("top_brands",topBrand);

            Log.e("TAG","object = "+mainObject.toString());

            callProfileUpdateApi(fullname,userBirthDate,zipCode,userAnniversary,phone,imageFilePth,mainObject.toString());
        }
        catch (Exception e)
        {Log.e("TAG","Error = "+e.getLocalizedMessage());}
    }

    private void callProfileUpdateApi(String fullname, String userBirthDate, String zipCode, String userAnniversary, String phone,String imageFilePth,String userInfo)
    {
        loaderDialog.show();
        RequestBody id = RequestBody.create(MediaType.parse("text/plain"), loginData.getUser().getId());
        RequestBody fName = RequestBody.create(MediaType.parse("text/plain"), fullname);
        RequestBody uBirthDate = RequestBody.create(MediaType.parse("text/plain"), userBirthDate);
        RequestBody zCode = RequestBody.create(MediaType.parse("text/plain"), zipCode);
        RequestBody uAnniversary = RequestBody.create(MediaType.parse("text/plain"), userAnniversary);
        RequestBody phone_ = RequestBody.create(MediaType.parse("text/plain"), phone);
        RequestBody uInfo = RequestBody.create(MediaType.parse("text/plain"), userInfo);
        MultipartBody.Part imgFile;
        if(imageFilePth != null)
        {imgFile = MultipartBody.Part.createFormData("profile_picture", imageFilePth, RequestBody.create(MediaType.parse("image/*"), new File(imageFilePth)));}
        else
        {imgFile = null;}


        Call<Object> call = application.getApis().getUserProfile(id,fName,uBirthDate,uAnniversary,phone_,imgFile,zCode,uInfo);
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG","response = "+x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(),dialogCommon,activity,application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if(jsonObject.has("status") && jsonObject.getBoolean("status"))
                    {
                        MyProfile myProfile = gson.fromJson(x,MyProfile.class);

                        loginData.getUser().setFirstName(myProfile.getUser().getFirstName());
                        loginData.getUser().setLastName(myProfile.getUser().getLastName());
                        loginData.getUser().setBirthdayDate(myProfile.getUser().getBirthdayDate());
                        loginData.getUser().setZipCode(myProfile.getUser().getZipCode());
                        loginData.getUser().setAnniversaryDate(myProfile.getUser().getAnniversaryDate());
                        loginData.getUser().setPhone(myProfile.getUser().getPhone());
                        loginData.getUser().setProfilePicture(myProfile.getUser().getProfilePicture());

                        application.getSharedPref().saveSession(Constant.LOGIN_DATA,gson.toJson(loginData));
                        dialogCommon.showDialog(jsonObject.getString("message"), new AlertDialogCommon.CallBackClickListener() {
                            @Override
                            public void OnDialogPositiveBtn() {
                                onBackPressed();
                            }

                            @Override
                            public void OnDialogNegativeBtn() {

                            }
                        });
                    }
                    else
                    {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                }
                catch (Exception e)
                {}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    private void callMyProfileApi()
    {
        loaderDialog.show();
        RequestBody id = RequestBody.create(MediaType.parse("text/plain"), loginData.getUser().getId());

        Call<Object> call = application.getApis().getUserProfile(id,null,null,null,null,null,null,null);
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG","response = "+x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(),dialogCommon,activity,application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if(jsonObject.has("status") && jsonObject.getBoolean("status"))
                    {


                        /*JSONObject issueObj = new JSONObject(x);
                        Iterator iterator = issueObj.keys();
                        while(iterator.hasNext()){
                            String key = (String)iterator.next();
                            Log.e("TAG","key = "+key);
                            Log.e("TAG","obj = "+issueObj.optJSONObject(key));
                            Log.e("TAG","array = "+issueObj.optJSONArray(key));
                            Log.e("TAG","String = "+issueObj.opt(key));
                        }*/


                        userProfile = gson.fromJson(x,MyProfile.class);
                        setTopBrandFavouriteStore();
                        addClothingFootwearDynamically();
                        addLikeInterestDynamically();
                        setValue();
                    }
                    else
                    {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                }
                catch (Exception e)
                {Log.e("TAG","Error = "+e.getLocalizedMessage());}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    private void setValue()
    {
        if(Global.isValidString(userProfile.getUser().getFirstName()))
        {binding.etFirstName.setText(userProfile.getUser().getFirstName()+" "+userProfile.getUser().getLastName());}
        if(Global.isValidString(userProfile.getUser().getLastName()))
        {binding.etLastName.setText(userProfile.getUser().getLastName());}
        if(Global.isValidString(userProfile.getUser().getBirthdayDate()))
        {
            userBirthDate = userProfile.getUser().getBirthdayDate();
            binding.tvBirthDate.setText(Utility.getDateInFormat(userProfile.getUser().getBirthdayDate()));
        }
        if(Global.isValidString(userProfile.getUser().getZipCode()))
        {binding.etZipCode.setText(userProfile.getUser().getZipCode());}
        if(Global.isValidString(userProfile.getUser().getAnniversaryDate()))
        {
            userAnniversary = userProfile.getUser().getAnniversaryDate();
            binding.tvAnniversaryDate.setText(Utility.getDateInFormat(userProfile.getUser().getAnniversaryDate()));
        }
        if(Global.isValidString(userProfile.getUser().getPhone()))
        {binding.etPhone.setText(userProfile.getUser().getPhone());}
        if(Global.isValidString(userProfile.getUser().getProfilePicture()))
        {Picasso.get().load(userProfile.getUser().getProfilePicture()).into(binding.ivProfile);}

                littleOneAdapter = new LittleOneAdapter(activity,userProfile.getLittleOne(),LittleOneAdapter.FROM_EDIT_PROFILE);
        binding.rvLittleOne.setAdapter(littleOneAdapter);
        binding.rvLittleOne.setLayoutManager(new LinearLayoutManager(activity));
        binding.rvLittleOne.setNestedScrollingEnabled(false);
    }

    private void setTopBrandFavouriteStore()
    {
        setUpBrandAutoComplete(userProfile.getTopBrands(),userProfile.getUser().getTopBrands());
        setUpStoreAutoComplete(userProfile.getFavouriteStore(),userProfile.getUser().getFavouriteStore());
    }

    //create like interest field dynamically
    private void addLikeInterestDynamically()
    {
        like_interest_list = new ArrayList<>();

        List<String> stringList;
        for(int i = 0;i<userProfile.getLikesInterest().size();i++)
        {
            stringList = new ArrayList<>();
            if(userProfile.getLikesInterest().get(i).getFieldType().equalsIgnoreCase(Constant.SPINNER))
            {
                stringList.add("Select...");
                for(int j = 0 ;j < userProfile.getLikesInterest().get(i).getData().size();j++)
                {
                    stringList.add(userProfile.getLikesInterest().get(i).getData().get(j));
                }
            }
            like_interest_list.add(stringList);
        }

        Spinner spinner;
        MyTextView textView;
        MyEditText myEditText;
        LinearLayout linearLayout;

        for(int i = 0;i<userProfile.getLikesInterest().size();i++)
        {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 0, (int) getResources().getDimension(R.dimen._15sdp));


            linearLayout = new LinearLayout(activity);
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            linearLayout.setLayoutParams(params);

            textView = new MyTextView(activity);
            textView.setText(userProfile.getLikesInterest().get(i).getFieldName());
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimension(R.dimen.text_hint));
            textView.setMediumTextView(textView);
            textView.setAllCaps(true);
            textView.setTextColor(getResources().getColor(R.color.textColor));
            linearLayout.addView(textView);

            if(userProfile.getLikesInterest().get(i).getFieldType().equalsIgnoreCase(Constant.SPINNER))
            {
                spinner = new Spinner(activity);
                spinner.setId(i);
                spinner.setTag(i);
                spinner.setOnItemSelectedListener(listener3);
                spinner.setBackground(getResources().getDrawable(R.drawable.ic_spinner));
                linearLayout.addView(spinner);

                ArrayAdapter<String> aa4 = new ArrayAdapter(activity,R.layout.item_spinner,clothing_footwear_list.get(i));
                aa4.setDropDownViewResource(R.layout.item_spinner_dropdown);
                spinner.setAdapter(aa4);
                for(int y = 0;y<userProfile.getLikesInterest().get(i).getData().size();y++)
                {
                    if(userProfile.getLikesInterest().get(i).getData().get(y).equalsIgnoreCase(userProfile.getLikesInterest().get(i).getUser_value()))
                    {
                        spinner.setSelection(y+1);
                        break;
                    }
                }
            }
            else if(userProfile.getLikesInterest().get(i).getFieldType().equalsIgnoreCase(Constant.TEXTVIEW))
            {
                myEditText = new MyEditText(activity);
                myEditText.setBackground(getResources().getDrawable(R.drawable.edt_bg_selector));
                myEditText.setHint(userProfile.getLikesInterest().get(i).getFieldName());
                myEditText.setMaxLines(1);
                myEditText.setPadding(0, (int) getResources().getDimension(R.dimen._5sdp),0,(int) getResources().getDimension(R.dimen._8sdp));
                myEditText.setSingleLine();
                myEditText.setTextColor(getResources().getColor(R.color.colorBlack));
                myEditText.setHintTextColor(getResources().getColor(R.color.textColorHint));
                myEditText.setTextSize((int) (getResources().getDimension(R.dimen._5sdp)));
                myEditText.setText(userProfile.getLikesInterest().get(i).getUser_value());
                linearLayout.addView(myEditText);
            }

            binding.llLikeInterest.addView(linearLayout);
        }
    }

    //create clothing footwear field dynamically
    private void addClothingFootwearDynamically()
    {
        clothing_footwear_list = new ArrayList<>();

        List<String> stringList;
        for(int i = 0;i<userProfile.getClothingFootware().size();i++)
        {
            stringList = new ArrayList<>();
            if(userProfile.getClothingFootware().get(i).getFieldType().equalsIgnoreCase(Constant.SPINNER))
            {
                stringList.add("Select...");
                for(int j = 0 ;j < userProfile.getClothingFootware().get(i).getData().size();j++)
                {
                    stringList.add(userProfile.getClothingFootware().get(i).getData().get(j));
                }
            }
            clothing_footwear_list.add(stringList);
        }

        Spinner spinner;
        MyTextView textView;
        MyEditText myEditText;
        LinearLayout linearLayout;

        for(int i = 0;i<userProfile.getClothingFootware().size();i++)
        {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 0, (int) getResources().getDimension(R.dimen._15sdp));

            linearLayout = new LinearLayout(activity);
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            linearLayout.setLayoutParams(params);


            textView = new MyTextView(activity);
            textView.setText(userProfile.getClothingFootware().get(i).getFieldName());
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimension(R.dimen.text_hint));
            textView.setMediumTextView(textView);
            textView.setAllCaps(true);
            textView.setTextColor(getResources().getColor(R.color.textColor));
            linearLayout.addView(textView);

            if(userProfile.getClothingFootware().get(i).getFieldType().equalsIgnoreCase(Constant.SPINNER))
            {
                spinner = new Spinner(activity);
                spinner.setId(i);
                spinner.setTag(i);
                spinner.setOnItemSelectedListener(listener2);
                spinner.setBackground(getResources().getDrawable(R.drawable.ic_spinner));
                linearLayout.addView(spinner);

                ArrayAdapter<String> aa4 = new ArrayAdapter(activity,R.layout.item_spinner,clothing_footwear_list.get(i));
                aa4.setDropDownViewResource(R.layout.item_spinner_dropdown);
                spinner.setAdapter(aa4);

                for(int y = 0;y<userProfile.getClothingFootware().get(i).getData().size();y++)
                {
                    if(userProfile.getClothingFootware().get(i).getData().get(y).equalsIgnoreCase(userProfile.getClothingFootware().get(i).getUser_value()))
                    {
                        spinner.setSelection(y+1);
                        break;
                    }
                }
            }
            else if(userProfile.getClothingFootware().get(i).getFieldType().equalsIgnoreCase(Constant.TEXTVIEW))
            {
                myEditText = new MyEditText(activity);
                myEditText.setBackground(getResources().getDrawable(R.drawable.edt_bg_selector));
                myEditText.setHint(userProfile.getClothingFootware().get(i).getFieldName());
                myEditText.setMaxLines(1);
                myEditText.setPadding(0, (int) getResources().getDimension(R.dimen._5sdp),0,(int) getResources().getDimension(R.dimen._8sdp));
                myEditText.setSingleLine();
                myEditText.setTextColor(getResources().getColor(R.color.colorBlack));
                myEditText.setHintTextColor(getResources().getColor(R.color.textColorHint));
                myEditText.setTextSize((int) (getResources().getDimension(R.dimen._5sdp)));
                myEditText.setText(userProfile.getClothingFootware().get(i).getUser_value());
                linearLayout.addView(myEditText);
            }

            binding.llClothingFootwear.addView(linearLayout);
        }
    }

    //set favourite store suggestion and user selected chip
    private void setUpStoreAutoComplete(List<String> favouriteStore, List<String> userSelected)
    {
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, favouriteStore);
        drawableWithCloseConfig = new ChipCloudConfig().selectMode(ChipCloud.SelectMode.multi).uncheckedTextColor(getResources().getColor(R.color.colorBlack)).showClose(Color.parseColor("#a6a6a6"), 500);

        storeChip = new ChipCloud(this, binding.flexBoxStore, drawableWithCloseConfig);
        binding.autoStoreName.setAdapter(adapter);
        binding.autoStoreName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (binding.autoStoreName.getRight() - binding.autoStoreName.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        Log.e("TAG","Click");
                        if(!binding.autoStoreName.getText().toString().trim().equalsIgnoreCase(""))
                        {
                            for(int i = 0;i<storeChip.getAllChips().size();i++)
                            {
                                if(storeChip.getAllChips().get(i).getLabel().equalsIgnoreCase(binding.autoStoreName.getText().toString().trim()))
                                {
                                    dialogCommon.showDialog(getString(R.string.msg_already_added));
                                    return true;
                                }
                            }
                            storeChip.addChip(binding.autoStoreName.getText().toString().trim(), null);
                            binding.autoStoreName.setText("");
                        }

                        return true;
                    }
                }
                return false;
            }
        });

        ///set selected value
        for(int i = 0;i<userSelected.size();i++)
        {
            storeChip.addChip(userSelected.get(i), null);
        }

        storeChip.setListener(new ChipListener() {
            @Override
            public void onChipCheckChanged(int index, String label, boolean checked, boolean userClick, Chip chip) {

            }

            @Override
            public void onChipDeleted(int index, String label, Chip chip) {
                storeChip.removeChip(index);
            }
        });

    }

    //set top brand suggestion and user selected chip
    private void setUpBrandAutoComplete(List<String> topBrands, List<String> userSelected)
    {
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, topBrands);
        drawableWithCloseConfig = new ChipCloudConfig().selectMode(ChipCloud.SelectMode.multi).uncheckedTextColor(getResources().getColor(R.color.colorBlack)).showClose(Color.parseColor("#a6a6a6"), 500);

        brandChip = new ChipCloud(this, binding.flexBoxBrand, drawableWithCloseConfig);
        binding.autoBrandName.setAdapter(adapter);
        binding.autoBrandName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (binding.autoBrandName.getRight() - binding.autoBrandName.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        Log.e("TAG","Click");
                        if(!binding.autoBrandName.getText().toString().trim().equalsIgnoreCase(""))
                        {
                            for(int i = 0;i<brandChip.getAllChips().size();i++)
                            {
                                if(brandChip.getAllChips().get(i).getLabel().equalsIgnoreCase(binding.autoBrandName.getText().toString().trim()))
                                {
                                    dialogCommon.showDialog(getString(R.string.msg_already_added));
                                    return true;
                                }
                            }
                            brandChip.addChip(binding.autoBrandName.getText().toString().trim(), null);
                            binding.autoBrandName.setText("");
                        }
                        return true;
                    }
                }
                return false;
            }
        });

        ///set selected value
        for(int i = 0;i<userSelected.size();i++)
        {
            brandChip.addChip(userSelected.get(i), null);
        }

        brandChip.setListener(new ChipListener() {
            @Override
            public void onChipCheckChanged(int index, String label, boolean checked, boolean userClick, Chip chip) {

            }

            @Override
            public void onChipDeleted(int index, String label, Chip chip) {
                brandChip.removeChip(index);
            }
        });
    }

    //set listener for clothing and footwear
    private AdapterView.OnItemSelectedListener listener2 = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            for(int i = 0;i<5;i++)
            {
                if(parent.getId() == i)
                {
                    if(clothing_footwear_list.get(i).get(position).equalsIgnoreCase("Select..."))
                    {((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColorHint));}
                    Log.e("TAG","11111111111111111");
                    break;
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    //set listener for like and interest
    private AdapterView.OnItemSelectedListener listener3 = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            for(int i = 0;i<5;i++)
            {
                if(parent.getId() == i)
                {
                    if(like_interest_list.get(i).get(position).equalsIgnoreCase("Select..."))
                    {((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColorHint));}
                    Log.e("TAG","11111111111111111");
                    break;
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        application.setCurrentActivity(activity);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
