package com.app.mylineup.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.app.mylineup.BuildConfig;
import com.app.mylineup.R;
import com.app.mylineup.databinding.ActivityRegisterBinding;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.Global;
import com.app.mylineup.other.Utility;
import com.app.mylineup.web_services.PrettyPrinter;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import org.json.JSONObject;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okio.Buffer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends BaseActivity
{
    private ActivityRegisterBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        init();
    }

    private void init()
    {
        binding.tvSignIn.setOnClickListener(v -> {
            onBackPressed();
        });

        binding.tvCreateAcc.setOnClickListener(v -> {
            checkValidation();
        });

        binding.etEmail.addTextChangedListener(new onTextChangeListener());
        binding.etPassword.addTextChangedListener(new onTextChangeListener());
        binding.etRePassword.addTextChangedListener(new onTextChangeListener());
        binding.etFullName.addTextChangedListener(new onTextChangeListener());
        binding.etMobile.addTextChangedListener(new onTextChangeListener());

        binding.etMobile.addTextChangedListener(new Utility.PhoneNumberChecker(binding.etMobile));


        setPrivacyPolicy();


    }

    private void setPrivacyPolicy()
    {
        SpannableString spanString = new SpannableString(getResources().getString(R.string.privacy_policy));
        Matcher matcher = Pattern.compile("Terms and Condition").matcher(spanString);
        Matcher matcher1 = Pattern.compile("Privacy Policy").matcher(spanString);

        while (matcher.find()) {
            ForegroundColorSpan foregroundSpan = new ForegroundColorSpan(Color.RED);
            spanString.setSpan(foregroundSpan, matcher.start(), matcher.end(), 0); //to highlight word havgin '@'
            final String tag = matcher.group(0);

            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    Log.e("TAG","acvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv");
                    //ffffffffffffff
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setColor(getResources().getColor(R.color.colorAccent));

                }
            };
            spanString.setSpan(clickableSpan, matcher.start(), matcher.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        while (matcher1.find()) {
            ForegroundColorSpan foregroundSpan = new ForegroundColorSpan(Color.RED);
            spanString.setSpan(foregroundSpan, matcher1.start(), matcher1.end(), 0); //to highlight word havgin '@'
            final String tag = matcher1.group(0);

            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    //fffffffffff
                    Log.e("TAG","ddddddddddddddddddddddddd");
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setColor(getResources().getColor(R.color.colorAccent));

                }
            };
            spanString.setSpan(clickableSpan, matcher1.start(), matcher1.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        binding.tvPrivacyPolicy.setText(spanString);
        binding.tvPrivacyPolicy.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void checkValidation()
    {
        if(!Global.isValidString(binding.etFullName.getText().toString()))
        {dialogCommon.showDialog(getString(R.string.please_enter_full_name));}
        else if(!Global.isEmailValidat(binding.etEmail.getText().toString()))
        {dialogCommon.showDialog(getString(R.string.please_enter_valid_email));}
        else if(!Global.isValidString(binding.etMobile.getText().toString()))
        {dialogCommon.showDialog(getString(R.string.please_enter_phone_number));}
        else if(!Global.isValidString(binding.etPassword.getText().toString()))
        {dialogCommon.showDialog(getString(R.string.please_enter_password));}
        else if(binding.etPassword.getText().toString().length() < 6)
        {dialogCommon.showDialog(getString(R.string.password_validation));}
        else if(!Global.isValidString(binding.etRePassword.getText().toString()))
        {dialogCommon.showDialog(getString(R.string.please_reenter_password));}
        else if(!binding.etPassword.getText().toString().equalsIgnoreCase(binding.etRePassword.getText().toString()))
        {dialogCommon.showDialog(getString(R.string.password_re_password_not_match));}
        else if(!binding.checkBoxPrivacy.isChecked())
        {dialogCommon.showDialog(getString(R.string.please_accept_terms_privacy_policy));}
        else
        {
            callRegisterApi();
        }
    }

    private void callRegisterApi()
    {
        loaderDialog.show();
        Call<Object> call = application.getApis().registerUser(binding.etFullName.getText().toString().trim(),binding.etMobile.getText().toString().trim(),
                binding.etEmail.getText().toString().trim(),binding.etPassword.getText().toString().trim(),
                "123456","1", Constant.newgpsLatitude,Constant.newgpsLongitude,android.os.Build.MODEL);
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG","response = "+x);
                loaderDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if(jsonObject.has("status") && jsonObject.getBoolean("status"))
                    {
                        binding.etRePassword.setText("");
                        binding.etPassword.setText("");
                        binding.etEmail.setText("");
                        dialogCommon.showDialog(jsonObject.getString("message"), new AlertDialogCommon.CallBackClickListener() {
                            @Override
                            public void OnDialogPositiveBtn() {
                                onBackPressed();
                            }

                            @Override
                            public void OnDialogNegativeBtn() {

                            }
                        });
                    }
                    else
                    {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                }
                catch (Exception e)
                {}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        application.setCurrentActivity(activity);
    }


    public class onTextChangeListener implements TextWatcher
    {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            Log.e("TAG","Text = "+s.toString());
            if(binding.etFullName.getText().toString().equalsIgnoreCase("") ||
                    binding.etMobile.getText().toString().equalsIgnoreCase("") ||
                    binding.etEmail.getText().toString().equalsIgnoreCase("") ||
                    binding.etPassword.getText().toString().equalsIgnoreCase("") ||
                    binding.etRePassword.getText().toString().equalsIgnoreCase(""))
            {binding.tvCreateAcc.setSelected(false);}
            else
            {binding.tvCreateAcc.setSelected(true);}
        }
    }
}
