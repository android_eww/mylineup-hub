package com.app.mylineup.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.mylineup.R;
import com.app.mylineup.adapter.FaqAdapter;
import com.app.mylineup.databinding.ActivityFaqBinding;

public class FaqActivity extends BaseActivity
{
    private ActivityFaqBinding binding;
    private FaqAdapter faqAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_faq);
        init();
    }

    private void init()
    {
        setHeader(getString(R.string.lbl_faq),null,null);

        faqAdapter = new FaqAdapter(activity);
        binding.rvFaq.setLayoutManager(new LinearLayoutManager(activity));
        binding.rvFaq.setAdapter(faqAdapter);
    }
}
