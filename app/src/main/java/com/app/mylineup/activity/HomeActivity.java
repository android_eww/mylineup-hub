package com.app.mylineup.activity;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.app.mylineup.R;
import com.app.mylineup.databinding.ActivityHomeBinding;
import com.app.mylineup.fragment.ConnectionFragment;
import com.app.mylineup.fragment.GiftFeedFragment;
import com.app.mylineup.fragment.NotificationFragment;
import com.app.mylineup.fragment.WishlistFragment;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.ResultActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.UCropFragment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class HomeActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener
{

    private ActivityHomeBinding binding;
    private WishlistFragment wishlistFragment;
    private GiftFeedFragment giftFeedFragment;
    private NotificationFragment notificationFragment;
    private ConnectionFragment connectionFragment;
    private FragmentTransaction transaction;

    private Uri imageUri;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        init();
    }

    private void init()
    {
        connectionFragment = new ConnectionFragment();
        notificationFragment = new NotificationFragment();
        giftFeedFragment = new GiftFeedFragment();
        wishlistFragment = new WishlistFragment();

        loadWishList();


        binding.navigation.setOnNavigationItemSelectedListener(this);
        binding.ivHexagon.setOnClickListener(v -> {
            openCamera();
        });

        Log.e("TAG","email = "+loginData.getUser().getEmail());
    }

    private void openCamera() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
        {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "New Picture");
            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, Constant.CAMERA_RESULT);
        }
         else {
            String[] PERMISSIONS = {
                    android.Manifest.permission.CAMERA,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
            };

            ActivityCompat.requestPermissions(this,PERMISSIONS,Constant.REQUEST_ID_MULTIPLE_PERMISSIONS);
        }
    }

    private void loadGiftFeed()
    {
        transaction = getSupportFragmentManager().beginTransaction();
        if(connectionFragment.isAdded())
        {transaction.hide(connectionFragment);}
        if(notificationFragment.isAdded())
        {transaction.hide(notificationFragment);}
        if(wishlistFragment.isAdded())
        {transaction.hide(wishlistFragment);}
        if(giftFeedFragment.isAdded())
        {transaction.show(giftFeedFragment);}
        else
        {transaction.add(R.id.frame, giftFeedFragment);}

        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commitAllowingStateLoss();
    }

    private void loadNotification()
    {
        transaction = getSupportFragmentManager().beginTransaction();
        if(connectionFragment.isAdded())
        {transaction.hide(connectionFragment);}
        if(giftFeedFragment.isAdded())
        {transaction.hide(giftFeedFragment);}
        if(wishlistFragment.isAdded())
        {transaction.hide(wishlistFragment);}
        if(notificationFragment.isAdded())
        {transaction.show(notificationFragment);}
        else
        {transaction.add(R.id.frame, notificationFragment);}

        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commitAllowingStateLoss();
    }

    private void loadConnection()
    {
        transaction = getSupportFragmentManager().beginTransaction();
        if(notificationFragment.isAdded())
        {transaction.hide(notificationFragment);}
        if(giftFeedFragment.isAdded())
        {transaction.hide(giftFeedFragment);}
        if(wishlistFragment.isAdded())
        {transaction.hide(wishlistFragment);}
        if(connectionFragment.isAdded())
        {transaction.show(connectionFragment);}
        else
        {transaction.add(R.id.frame, connectionFragment);}

        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commitAllowingStateLoss();
    }

    private void loadWishList()
    {
        transaction = getSupportFragmentManager().beginTransaction();
        if(connectionFragment.isAdded())
        {transaction.hide(connectionFragment);}
        if(notificationFragment.isAdded())
        {transaction.hide(notificationFragment);}
        if(giftFeedFragment.isAdded())
        {transaction.hide(giftFeedFragment);}
        if(wishlistFragment.isAdded())
        {transaction.show(wishlistFragment);}
        else
        {transaction.add(R.id.frame, wishlistFragment);}

        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId()) {
            case R.id.i_wish:
                loadWishList();
                break;

            case R.id.i_gift:
                loadGiftFeed();
                break;

            case R.id.i_notification:
                loadNotification();
                break;

            case R.id.i_conection:
                loadConnection();
                break;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            if(requestCode == Constant.REQUEST_ID_MULTIPLE_PERMISSIONS)
            {
                if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                {
                    openCamera();
                }
            }
        }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constant.CAMERA_RESULT)
            {
                /*Bitmap photo = null;
                try {
                    photo = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                }
                catch (Exception e)
                {}
                final Uri selectedUri = storeImage(photo);*/
                if (imageUri != null) {
                    startCrop(imageUri);
                } else {
                    Toast.makeText(activity, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            }
            else if (requestCode == UCrop.REQUEST_CROP)
            {
                handleCropResult(data);
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }
    }

    private Uri storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.e("TAG", "Error creating media file, check storage permissions: ");// e.getMessage());
            return null; }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();

            Uri uri = Uri.fromFile(pictureFile);

            return uri;
        } catch (FileNotFoundException e) {
            Log.e("TAG", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.e("TAG", "Error accessing file: " + e.getMessage());
        }
        return null;
    }

    private  File getOutputMediaFile(){

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/Android/data/" + getApplicationContext().getPackageName() + "/Files");
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName="MI_"+ timeStamp +".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            Intent intent = new Intent(activity, CreateItemActivity.class);
            intent.setData(resultUri);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            //ResultActivity.startWithUri(activity, resultUri);

        } else {
            Toast.makeText(activity, R.string.toast_cannot_retrieve_cropped_image, Toast.LENGTH_SHORT).show();
        }
    }

    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Log.e("TAG", "handleCropError: ", cropError);
            Toast.makeText(activity, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(activity, R.string.toast_unexpected_error, Toast.LENGTH_SHORT).show();
        }
    }


    private void startCrop(@NonNull Uri uri)
    {
        String destinationFileName = "SampleCropImage";
        destinationFileName += ".jpg";
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), destinationFileName)));
        uCrop = basisConfig(uCrop);
        uCrop = advancedConfig(uCrop);

        uCrop.start(activity);

    }

    private UCrop basisConfig(@NonNull UCrop uCrop) {

        uCrop = uCrop.useSourceImageAspectRatio();
        //uCrop = uCrop.withAspectRatio(1, 1);//radio_square
        return uCrop;
    }

    private UCrop advancedConfig(@NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();

        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);

        options.setCompressionQuality(90);

        options.setHideBottomControls(false);
        options.setFreeStyleCropEnabled(false);

        return uCrop.withOptions(options);
    }

    @Override
    protected void onResume() {
        super.onResume();
        application.setCurrentActivity(activity);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
