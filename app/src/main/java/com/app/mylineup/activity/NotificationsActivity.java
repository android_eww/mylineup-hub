package com.app.mylineup.activity;

import android.os.Bundle;
import android.util.Log;

import com.app.mylineup.R;
import com.app.mylineup.databinding.ActivityNotificationBinding;
import com.app.mylineup.other.Constant;
import com.app.mylineup.web_services.PrettyPrinter;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationsActivity extends BaseActivity
{
    private ActivityNotificationBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification);
        init();
    }

    private void init()
    {
        setHeader(getString(R.string.notifications),null,null);

        if(loginData.getNotificationSetting().getNewConnections() != 0) {
            binding.switchNewConnection.setChecked(true);
        }
        if (loginData.getNotificationSetting().getWeeklyUpdates() != 0) {
            binding.switchWeeklyUpdate.setChecked(true);
        }
        if (loginData.getNotificationSetting().getItemPurchased() != 0) {
            binding.switchPurchasedFromWishlist.setChecked(true);
        }
        if (loginData.getNotificationSetting().getAppNotification() != 0) {
            binding.switchNotification.setChecked(true);
        }

        binding.llNewConnection.setOnClickListener(view -> {
            binding.switchNewConnection.setChecked(!binding.switchNewConnection.isChecked());
            callUpdateNotificationApi("new_connections",binding.switchNewConnection.isChecked() ? 1 : 0 );
        });
        binding.llWeeklyUpdate.setOnClickListener(view -> {
            binding.switchWeeklyUpdate.setChecked(!binding.switchWeeklyUpdate.isChecked());
            callUpdateNotificationApi("weekly_updates",binding.switchWeeklyUpdate.isChecked() ? 1 : 0);
        });
        binding.llPurchasedFromWishlist.setOnClickListener(view -> {
            binding.switchPurchasedFromWishlist.setChecked(!binding.switchPurchasedFromWishlist.isChecked());
            callUpdateNotificationApi("item_purchased",binding.switchPurchasedFromWishlist.isChecked() ? 1 : 0);
        });
        binding.llNotification.setOnClickListener(view -> {
            binding.switchNotification.setChecked(!binding.switchNotification.isChecked());
            callUpdateNotificationApi("app_notification",binding.switchNotification.isChecked() ? 1 : 0);
        });

    }

    private void callUpdateNotificationApi(String key, int status)
    {
        loaderDialog.show();
        Call<Object> call = application.getApis().updateNotificationSetting(loginData.getUser().getId(),key,status+"");
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG","response = "+x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(),dialogCommon,activity,application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if(jsonObject.has("status") && jsonObject.getBoolean("status"))
                    {
                        if(key.equalsIgnoreCase("new_connections"))
                        {loginData.getNotificationSetting().setNewConnections(status);}
                        else if(key.equalsIgnoreCase("weekly_updates"))
                        {loginData.getNotificationSetting().setWeeklyUpdates(status);}
                        else if(key.equalsIgnoreCase("item_purchased"))
                        {loginData.getNotificationSetting().setItemPurchased(status);}
                        else if(key.equalsIgnoreCase("app_notification"))
                        {loginData.getNotificationSetting().setAppNotification(status);}

                        Log.e("TAG","login data = "+gson.toJson(loginData));
                        application.getSharedPref().saveSession(Constant.LOGIN_DATA,gson.toJson(loginData));
                    }
                    else
                    {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                        if(key.equalsIgnoreCase("new_connections"))
                        {binding.switchNewConnection.setChecked(!binding.switchNewConnection.isChecked());}
                        else if(key.equalsIgnoreCase("weekly_updates"))
                        {binding.switchWeeklyUpdate.setChecked(!binding.switchWeeklyUpdate.isChecked());}
                        else if(key.equalsIgnoreCase("item_purchased"))
                        {binding.switchPurchasedFromWishlist.setChecked(!binding.switchPurchasedFromWishlist.isChecked());}
                        else if(key.equalsIgnoreCase("app_notification"))
                        {binding.switchNotification.setChecked(!binding.switchNotification.isChecked());}
                    }
                }
                catch (Exception e)
                {}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        application.setCurrentActivity(activity);
    }
}
