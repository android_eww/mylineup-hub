package com.app.mylineup.activity;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.app.mylineup.R;
import com.app.mylineup.adapter.WishListAdapter;
import com.app.mylineup.databinding.ActivityWishlistBinding;
import com.app.mylineup.other.Constant;
import com.yalantis.ucrop.UCrop;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import java.io.File;

public class WishlistActivity extends BaseActivity implements BaseActivity.onHeaderAction
{
    private ActivityWishlistBinding binding;
    private WishListAdapter wishListAdapter;

    private Uri imageUri;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_wishlist);
        init();
    }

    private void init()
    {
        if(getIntent().getExtras() != null && getIntent().getStringExtra(Constant.FROM).equalsIgnoreCase(Constant.FROM_USER_PROFILE))
        {
            binding.tvAddItem.setVisibility(View.GONE);
        }
        //setHeader("23rd Birthday",R.drawable.ic_share_icon,R.drawable.ic_edit_icon);
        setHeader("23rd Birthday",null,null);
        setInterFace(this::onClick);

        wishListAdapter = new WishListAdapter(activity,  Constant.FROM_WISHLIST_DETAIL);
        binding.rvWishList.setLayoutManager(new GridLayoutManager(activity,2));
        binding.rvWishList.setAdapter(wishListAdapter);


        binding.tvAddItem.setOnClickListener(v -> {
            openCamera();
        });

    }

    @Override
    public void onClick(int id)
    {
        if(R.id.ivRight2 == id)
        {
            Log.e("TAG","Share");
        }
        else if(R.id.ivRight == id)
        {
            Log.e("TAG","Edit");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        application.setCurrentActivity(activity);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == Constant.REQUEST_ID_MULTIPLE_PERMISSIONS)
        {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            {
                openCamera();
            }
        }
    }

    private void openCamera() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
        {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "New Picture");
            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, Constant.CAMERA_RESULT);
        }
        else {
            String[] PERMISSIONS = {
                    android.Manifest.permission.CAMERA,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
            };

            ActivityCompat.requestPermissions(this,PERMISSIONS,Constant.REQUEST_ID_MULTIPLE_PERMISSIONS);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constant.CAMERA_RESULT)
            {
                /*Bitmap photo = null;
                try {
                    photo = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                }
                catch (Exception e)
                {}
                final Uri selectedUri = storeImage(photo);*/
                if (imageUri != null) {
                    startCrop(imageUri);
                } else {
                    Toast.makeText(activity, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            }
            else if (requestCode == UCrop.REQUEST_CROP)
            {
                handleCropResult(data);
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }
    }

    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            Intent intent = new Intent(activity, CreateItemActivity.class);
            intent.setData(resultUri);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            //ResultActivity.startWithUri(activity, resultUri);

        } else {
            Toast.makeText(activity, R.string.toast_cannot_retrieve_cropped_image, Toast.LENGTH_SHORT).show();
        }
    }

    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Log.e("TAG", "handleCropError: ", cropError);
            Toast.makeText(activity, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(activity, R.string.toast_unexpected_error, Toast.LENGTH_SHORT).show();
        }
    }

    private void startCrop(@NonNull Uri uri)
    {
        String destinationFileName = "SampleCropImage";
        destinationFileName += ".jpg";
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), destinationFileName)));
        uCrop = basisConfig(uCrop);
        uCrop = advancedConfig(uCrop);

        uCrop.start(activity);

    }

    private UCrop basisConfig(@NonNull UCrop uCrop) {

        uCrop = uCrop.useSourceImageAspectRatio();
        //uCrop = uCrop.withAspectRatio(1, 1);//radio_square
        return uCrop;
    }

    private UCrop advancedConfig(@NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();

        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);

        options.setCompressionQuality(90);

        options.setHideBottomControls(false);
        options.setFreeStyleCropEnabled(false);

        return uCrop.withOptions(options);
    }
}
