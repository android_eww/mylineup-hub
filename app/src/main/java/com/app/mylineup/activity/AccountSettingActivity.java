package com.app.mylineup.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import com.app.mylineup.R;
import com.app.mylineup.adapter.LittleOneAdapter;
import com.app.mylineup.databinding.ActivityAccountSettingBinding;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.interfaces.CallbackLittleOne;
import com.app.mylineup.interfaces.LittleOneSingleton;
import com.app.mylineup.other.Constant;
import com.app.mylineup.pojo.myProfile.LittleOne;
import com.app.mylineup.pojo.myProfile.MyProfile;
import com.app.mylineup.web_services.PrettyPrinter;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountSettingActivity extends BaseActivity
{
    protected static AccountSettingActivity activity;
    protected static boolean isChange = false;
    private ActivityAccountSettingBinding binding;
    private LittleOneAdapter littleOneAdapter;
    protected MyProfile myProfile;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = AccountSettingActivity.this;
        //LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, new IntentFilter("LITTLE_ONE_CHANGE"));
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_account_setting);
        init();
    }

    private void init()
    {
        setHeader(getString(R.string.account_setting),null,null);
        isChange = false;

        binding.llProfile.setOnClickListener(v -> {
            Intent intent = new Intent(activity, UserProfileActivity.class);
            intent.putExtra(Constant.FROM,Constant.FROM_MYPROFILE);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        });


        binding.llNotification.setOnClickListener(v -> {
            Intent intent = new Intent(activity, NotificationsActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        });

        binding.llChangePassword.setOnClickListener(view -> {
            Intent intent = new Intent(activity, ChangePasswordActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        });

        binding.llFaq.setOnClickListener(v -> {
            Intent intent = new Intent(activity, FaqActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        });

        binding.llCreateLittleOne.setOnClickListener(view -> {

            LittleOneSingleton.get(activity, new CallbackLittleOne() {
                @Override
                public void editLittleOne(String firstName, String lastName) {
                }
                @Override
                public void deleteLittleOne() {
                }
                @Override
                public void hideLittleOne() {
                }
                @Override
                public void addLittleOne(LittleOne littleOne) {
                    myProfile.getLittleOne().add(littleOne);
                    littleOneAdapter.notifyDataSetChanged();
                }
            });
            Intent intent = new Intent(activity, CreateLittleOneActivirt.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        });

        binding.tvSignOut.setOnClickListener(view -> {
            dialogCommon.showDialog(getString(R.string.app_name), getString(R.string.msg_logout), getString(R.string.ok), getString(R.string.label_cancel), true, true, new AlertDialogCommon.CallBackClickListener() {
                @Override
                public void OnDialogPositiveBtn() {
                    callLogoutApi();
                }

                @Override
                public void OnDialogNegativeBtn() {

                }
            });
        });

        callLittleOneApi();
    }

    private void callLittleOneApi()
    {
        loaderDialog.show();
        Call<Object> call = application.getApis().getLittleOne(loginData.getUser().getId());
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG","response = "+x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(),dialogCommon,activity,application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if(jsonObject.has("status") && jsonObject.getBoolean("status"))
                    {
                        myProfile = gson.fromJson(x,MyProfile.class);
                        setAdapter();
                    }
                }
                catch (Exception e)
                {}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    private void setAdapter()
    {
        littleOneAdapter = new LittleOneAdapter(activity,myProfile.getLittleOne(),LittleOneAdapter.FROM_ACCOUNT_SETTING);
        binding.rvLittleOne.setAdapter(littleOneAdapter);
        binding.rvLittleOne.setLayoutManager(new LinearLayoutManager(activity));
        binding.rvLittleOne.setNestedScrollingEnabled(false);
    }

    private void callLogoutApi()
    {
        loaderDialog.show();
        Call<Object> call = application.getApis().logout(loginData.getUser().getId());
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG","response = "+x);
                Log.e("TAG","code = "+response.code());
                Log.e("TAG","msg = "+response.message());
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(),dialogCommon,activity,application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if(jsonObject.has("status") && jsonObject.getBoolean("status"))
                    {
                        application.getSharedPref().saveSession(Constant.IS_LOGIN,"");
                        application.getSharedPref().clearAll();
                        Intent intent = new Intent(activity,LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                    }
                    else
                    {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                }
                catch (Exception e)
                {}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    protected void updateLittleOneList(String littleOneId,LittleOne littleOne,String firstName,String lastName)
    {
        if(littleOneId != null)
        {
            for(int i =0;i<myProfile.getLittleOne().size();i++)
            {
                if(myProfile.getLittleOne().get(i).getId().equalsIgnoreCase(littleOneId)){
                    myProfile.getLittleOne().get(i).setFirstName(firstName);
                    myProfile.getLittleOne().get(i).setLastName(lastName);
                    littleOneAdapter.notifyDataSetChanged();
                    break;
                }
            }
        }
        else
        {
            myProfile.getLittleOne().add(littleOne);
            littleOneAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        application.setCurrentActivity(activity);
        if(isChange)
        {isChange = !isChange;
            callLittleOneApi();
        }
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean isEdit = intent.getBooleanExtra("edit",false);
            Log.e("TAG","AccountSettingActivity");
            if(isEdit)
            {
                for(int i =0;i<myProfile.getLittleOne().size();i++)
                {
                    if(myProfile.getLittleOne().get(i).getId().equalsIgnoreCase(intent.getStringExtra("littleOneId"))){
                        myProfile.getLittleOne().get(i).setFirstName(intent.getStringExtra("firstName"));
                        myProfile.getLittleOne().get(i).setLastName(intent.getStringExtra("lastName"));
                        littleOneAdapter.notifyDataSetChanged();
                        break;
                    }
                }
            }
            else
            {
                LittleOne littleOne = (LittleOne)intent.getSerializableExtra("data");
                myProfile.getLittleOne().add(littleOne);
                littleOneAdapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
    }
}
