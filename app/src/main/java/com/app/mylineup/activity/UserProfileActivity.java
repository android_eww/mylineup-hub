package com.app.mylineup.activity;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.app.mylineup.R;
import com.app.mylineup.adapter.TabAdapter;
import com.app.mylineup.databinding.ActivityUserProfileBinding;
import com.app.mylineup.fragment.AboutMeFragment;
import com.app.mylineup.fragment.ActivityFragment;
import com.app.mylineup.fragment.UserWishlistFragment;
import com.app.mylineup.other.Constant;
import com.google.android.material.tabs.TabLayout;

import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

public class UserProfileActivity extends ImageActivity implements BaseActivity.onHeaderAction {

    private ActivityUserProfileBinding binding;

    private TabAdapter adapter;
    private boolean isMyAcc= true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(activity,R.layout.activity_user_profile);
        init();
    }

    private void init()
    {
        if(getIntent().getStringExtra(Constant.FROM).equalsIgnoreCase(Constant.FROM_USER_PROFILE))
        {
            binding.llSelectImg.setVisibility(View.GONE);
            isMyAcc = false;

            setHeader("User Profile",null,null/*R.drawable.ic_dots*/);
            //setInterFace(this::onClick);
        }
        else
        {
            setHeader(getString(R.string.my_profile),null,R.drawable.ic_edit_icon);
            setInterFace(this::onClick);
        }

        adapter = new TabAdapter(getSupportFragmentManager());
        adapter.addFragment(new UserWishlistFragment(isMyAcc), "Wishlists (3)");
        adapter.addFragment(new ActivityFragment(), "Activity");
        adapter.addFragment(new AboutMeFragment(), "About Me");

        binding.viewPager.setAdapter(adapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);
        binding.viewPager.setOffscreenPageLimit(3);

        binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                ///binding.viewPager.
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        binding.llSelectImg.setOnClickListener(v -> {
            /*callbackImage.openImageChooser((imageFilePath, imageUri, cameraPhotoOrientation) -> {
                this.imageUri = imageUri;
                binding.ivImage.setImageURI(imageUri);
                //this.imageFilePth = imageFilePath;
            },false);*/
        });
    }

    @Override
    public void onClick(int id)
    {
        if(R.id.ivRight2 == id)
        {
            Log.e("TAG","Share");
        }
        else if(R.id.ivRight == id)
        {
            Log.e("TAG","Edit");

            Intent intent = new Intent(activity, EditProfilerActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        application.setCurrentActivity(activity);
    }
}
