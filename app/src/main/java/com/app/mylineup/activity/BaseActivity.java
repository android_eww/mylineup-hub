package com.app.mylineup.activity;

import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.app.mylineup.R;
import com.app.mylineup.application.Application;
import com.app.mylineup.custom_view.MyTextView;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.dialog.AlertListDialog;
import com.app.mylineup.dialog.LoaderDialog;
import com.app.mylineup.interfaces.CallbackImage;
import com.app.mylineup.other.Constant;
import com.app.mylineup.pojo.loginData.LoginData;
import com.balysv.materialripple.MaterialRippleLayout;
import com.google.gson.Gson;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity
{
    public BaseActivity activity;
    protected Gson gson;

    private ImageView ivBack;
    private MyTextView tvHeader;
    private ImageView ivRight2,ivRight;
    private MaterialRippleLayout materialRight2,materialRight;
    private onHeaderAction onHeaderAction;

    protected CallbackImage callbackImage;
    protected AlertDialogCommon dialogCommon;
    protected AlertListDialog listDialog;

    protected LoaderDialog loaderDialog;
    protected Application application;
    protected LoginData loginData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        gson = new Gson();

        dialogCommon = new AlertDialogCommon(activity);
        loaderDialog = new LoaderDialog(activity);
        listDialog = new AlertListDialog(activity);

        try {
            callbackImage= (CallbackImage) activity;
        }
        catch (ClassCastException e){
            e.printStackTrace();
        }
        application = Application.getInstance();

        loginData = application.getSharedPref().getUserSession(Constant.LOGIN_DATA);
        init();
    }

    private void init()
    {

    }

    public void setHeader(String title,Object share,Object edit)
    {
        tvHeader = findViewById(R.id.tvHeader);
        ivBack = findViewById(R.id.ivBack);
        ivRight2 = findViewById(R.id.ivRight2);
        ivRight = findViewById(R.id.ivRight);

        materialRight2 = findViewById(R.id.materialRight2);
        materialRight = findViewById(R.id.materialRight);

        if(share != null)
        {
            ivRight2.setImageResource((int)share);
            ivRight2.setVisibility(View.VISIBLE);
        }
        else {materialRight2.setVisibility(View.GONE);}

        if(edit != null)
        {
            ivRight.setImageResource((int)edit);
            ivRight.setVisibility(View.VISIBLE);
        }else {materialRight.setVisibility(View.GONE);}

        ivRight.setOnClickListener(v -> {
            if(onHeaderAction == null)return;
            onHeaderAction.onClick(R.id.ivRight);
        });
        ivRight2.setOnClickListener(v -> {
            if(onHeaderAction == null)return;
            onHeaderAction.onClick(R.id.ivRight2);
        });

        ivBack.setOnClickListener(v -> {
            onBackPressed();
        });

        tvHeader.setText(title);
    }


    public void setInterFace(onHeaderAction onHeaderAction)
    {
        this.onHeaderAction = onHeaderAction;
    }

    public interface onHeaderAction
    {
        void onClick(int id);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void hideKeyboardFrom() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}
