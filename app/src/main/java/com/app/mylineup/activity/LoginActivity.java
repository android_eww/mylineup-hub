package com.app.mylineup.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;

import com.app.mylineup.BuildConfig;
import com.app.mylineup.R;
import com.app.mylineup.application.Application;
import com.app.mylineup.databinding.ActivityLoginBinding;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.Global;
import com.app.mylineup.pojo.loginData.LoginData;
import com.app.mylineup.web_services.PrettyPrinter;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends LocationActivity
{
    private ActivityLoginBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        init();
    }

    private void init()
    {
        getLastKnownLocation();

        binding.tvSignUp.setOnClickListener(v -> {
            Intent intent = new Intent(activity, RegisterActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        });

        binding.tvSignIn.setOnClickListener(v -> {
            checkValidation();
        });

        binding.tvForgotPassword.setOnClickListener(v -> {
            Intent intent = new Intent(activity, ForgotPasswordActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        });

        binding.etEmailAddress.addTextChangedListener(new onTextChangeListener());
        binding.etPassword.addTextChangedListener(new onTextChangeListener());

    }

    private void checkValidation()
    {
        if(!Global.isEmailValidat(binding.etEmailAddress.getText().toString()))
        {dialogCommon.showDialog(getString(R.string.please_enter_valid_email));}
        else if(!Global.isValidString(binding.etPassword.getText().toString()))
        {dialogCommon.showDialog(getString(R.string.please_enter_password));}
        else
        { callLoginApi(); }
    }

    private void callLoginApi()
    {
        loaderDialog.show();
        Call<Object> call = application.getApis().loginUser(binding.etEmailAddress.getText().toString().trim(),binding.etPassword.getText().toString().trim(),
                "123456","1", Constant.newgpsLatitude,Constant.newgpsLongitude);
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG","response = "+x);
                loaderDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if(jsonObject.has("status") && jsonObject.getBoolean("status"))
                    {
                        LoginData loginData = gson.fromJson(x,LoginData.class);

                        application.getSharedPref().saveSession(Constant.IS_LOGIN,"1");
                        application.getSharedPref().saveSession(Constant.LOGIN_DATA,x);
                        application.getSharedPref().saveSession(Constant.HEADER_KEY,"X-API-KEY");
                        application.getSharedPref().saveSession(Constant.HEADER_VALUE,loginData.getUser().getApiKey());

                        Intent intent = new Intent(activity, HomeActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                        finish();
                    }
                    else
                    {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                }
                catch (Exception e)
                {}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        application.setCurrentActivity(activity);
        application.registerReceiver();
    }

    public class onTextChangeListener implements TextWatcher
    {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            Log.e("TAG","Text = "+s.toString());
            if(binding.etEmailAddress.getText().toString().equalsIgnoreCase("") ||
                    binding.etPassword.getText().toString().equalsIgnoreCase(""))
            {binding.tvSignIn.setSelected(false);}
            else
            {binding.tvSignIn.setSelected(true);}
        }
    }
}
