package com.app.mylineup.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.mylineup.R;
import com.app.mylineup.adapter.IntroSliderAdapter;
import com.app.mylineup.databinding.ActivityIntroSliderBinding;
import com.app.mylineup.pojo.IntroBeen;

import java.util.ArrayList;
import java.util.List;

public class IntroSliderActivity extends BaseActivity
{
    private ActivityIntroSliderBinding binding;
    private List<IntroBeen> list = new ArrayList<>();
    private IntroSliderAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_intro_slider);
        init();
    }

    private void init()
    {
        list.add(new IntroBeen(R.drawable.intro1,"Add Your Gift In Wishlist","Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s."));
        list.add(new IntroBeen(R.drawable.intro2,"Online Shop Your Gifts","Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s."));
        list.add(new IntroBeen(R.drawable.intro3,"Giveway","Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s."));

        adapter = new IntroSliderAdapter(activity, list, new IntroSliderAdapter.ViewPagerListener() {
            @Override
            public void onNext(int pos) {
                if(pos==(list.size()-1)) {
                    Intent intent = new Intent(activity, LoginActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                    finish();
                }
                else {
                    binding.viewPagerIntro.setCurrentItem(pos+1);
                }
            }
        });
        binding.viewPagerIntro.setAdapter(adapter);
    }
}
