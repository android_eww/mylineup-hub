package com.app.mylineup.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.mylineup.R;
import com.app.mylineup.adapter.CustomSpinner;
import com.app.mylineup.adapter.TabAdapter;
import com.app.mylineup.databinding.ActivityCreateWishlistBinding;
import com.app.mylineup.fragment.CreateWishlistFragOne;
import com.app.mylineup.fragment.CreateWishlistFragTwo;
import com.app.mylineup.other.Global;
import com.app.mylineup.other.Utility;
import com.app.mylineup.pojo.connections.Connection;
import com.google.android.material.tabs.TabLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

public class CreateWishListActivity extends BaseActivity
{
    public static ActivityCreateWishlistBinding binding;

    private boolean isFirst = true;

    private TabAdapter adapter;
    private List<Connection> connections = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_create_wishlist);
        init();
    }

    private void init()
    {
        setHeader(getString(R.string.lbl_create_wishlist_),null,null);

        adapter = new TabAdapter(getSupportFragmentManager(),false);
        adapter.addFragment(new CreateWishlistFragOne(), "Details");
        //adapter.addFragment(new CreateWishlistFragTwo(), "invites");

        binding.viewPager.setAdapter(adapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);
        binding.viewPager.setOffscreenPageLimit(2);

        binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                Log.e("TAG","onPageSelected = "+position);
                Fragment fragment = (Fragment) adapter.instantiateItem(binding.viewPager, position);
                if(fragment instanceof CreateWishlistFragOne)
                {
                    CreateWishlistFragOne fragOne = (CreateWishlistFragOne)fragment;
                    fragOne.setList(connections);
                    Log.e("TAG","CreateWishlistFragOne");
                }
                else if(fragment instanceof CreateWishlistFragTwo)
                {
                    CreateWishlistFragTwo fragTwo = (CreateWishlistFragTwo)fragment;
                    Log.e("TAG","CreateWishlistFragTwo");
                    connections =  fragTwo.getList();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        application.setCurrentActivity(activity);
    }
}
