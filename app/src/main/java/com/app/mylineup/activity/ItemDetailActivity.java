package com.app.mylineup.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.app.mylineup.R;
import com.app.mylineup.databinding.ActivityItemDetailBinding;
import com.app.mylineup.databinding.DialogMarkAsPurchasedBinding;
import com.app.mylineup.dialog.AlertListDialog;
import com.app.mylineup.other.Global;
import com.app.mylineup.other.Utility;
import com.app.mylineup.pojo.AboutMeBeen;
import com.app.mylineup.pojo.CommonPojo;
import com.app.mylineup.pojo.OnlyString;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ItemDetailActivity extends BaseActivity implements BaseActivity.onHeaderAction {

    private ActivityItemDetailBinding binding;
    private BottomSheetDialog bottomSheerDialog;

    private DialogMarkAsPurchasedBinding parentView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_item_detail);
        init();
    }

    private void init()
    {
        setHeader("Item Details",R.drawable.ic_share_icon,R.drawable.ic_dots);
        setInterFace(this::onClick);

        bottomSheerDialog = new BottomSheetDialog(activity);
        //View parentView = View.inflate(activity, R.layout.dialog_mark_as_purchased, null);
        parentView = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.dialog_mark_as_purchased,null, false);
        bottomSheerDialog.setContentView(parentView.getRoot());
        //((View) parentView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));

        binding.tvMarkAsSubmit.setOnClickListener(v -> {

            ((View) parentView.getRoot().getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
            bottomSheerDialog.show();

        });

        parentView.tvDate.setOnClickListener(v -> {
            Global.openDatePicker(activity, false, false, new Global.CallbackDateSelect() {
                @Override
                public void OnDateSelect(Calendar myCalendar) {
                    SimpleDateFormat sdf = new SimpleDateFormat(Utility.dayDate);
                    int day = Integer.parseInt(sdf.format(myCalendar.getTime()));
                    sdf = new SimpleDateFormat("MMMM dd, yyyy");
                    String p = sdf.format(myCalendar.getTime());
                    int position = p.indexOf(',');
                    String date = p.substring(0,position)+Utility.getDayOfMonthSuffix(day)+p.substring(position,p.length());
                    parentView.tvDate.setText(date);
                }
            });
        });

        parentView.tvCancel.setOnClickListener(v -> {
            bottomSheerDialog.dismiss();
        });

        parentView.tvConfirm.setOnClickListener(v -> {
            bottomSheerDialog.dismiss();
        });

        binding.llTags.setOnClickListener(v -> {
            Intent intent = new Intent(activity, WishlistActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        });
    }

    @Override
    public void onClick(int id)
    {
        if(R.id.ivRight2 == id)
        {
            Log.e("TAG","Share");
        }
        else if(R.id.ivRight == id)
        {
            Log.e("TAG","Edit");
            //dialogCommon.showDialog("Title","xyz","Ok","Cancel",true,true,null);

            ArrayList<CommonPojo> aboutMeBeen = new ArrayList<>();
            aboutMeBeen.add(new OnlyString(getString(R.string.edit_item)));
            aboutMeBeen.add(new OnlyString(getString(R.string.delete_item)));

            listDialog.showDialog("",true,getString(R.string.label_cancel),aboutMeBeen, new AlertListDialog.CallBackClickListener() {
                @Override
                public void OnItemSelected(CommonPojo commonPojo, int position) {
                    if(commonPojo instanceof OnlyString)
                    {
                        OnlyString been = (OnlyString) commonPojo;
                        Log.e("TAG","item = "+been.getTitle());
                    }
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        application.setCurrentActivity(activity);
    }
}
