package com.app.mylineup.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.app.mylineup.R;
import com.app.mylineup.adapter.CustomSpinner;
import com.app.mylineup.databinding.ActivityCreateItemBinding;
import com.app.mylineup.interfaces.CallbackImage;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

public class CreateItemActivity extends ImageActivity
{
    private ActivityCreateItemBinding binding;
    private boolean isFirstSize = true;
    private boolean isFirstCategory = true;
    private boolean isFirstWishlist = true;

    String[] wishlist ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_create_item);
        init();
    }

    private void init()
    {
        setHeader(getString(R.string.lbl_create_new_item),null,null);

        Uri resultUri = getIntent().getData();
        if(resultUri != null)
        {
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        String[] country = { "Select...","Footwear", "Food", "Other"};
        ArrayAdapter aa = new ArrayAdapter(this,R.layout.item_spinner,country);
        aa.setDropDownViewResource(R.layout.item_spinner_dropdown);
        binding.spinnerCategory.setAdapter(aa);
        binding.spinnerCategory.setOnItemSelectedListener(listener);

        String[] size = { "Select...","10.5 M(US)", "11.5 M(US)", "Other"};
        ArrayAdapter aa1 = new ArrayAdapter(this,R.layout.item_spinner,size);
        aa1.setDropDownViewResource(R.layout.item_spinner_dropdown);
        binding.spinnerSize.setAdapter(aa1);
        binding.spinnerSize.setOnItemSelectedListener(listener);

        wishlist = new String[]{ "Select...","23rd Birthday, Christmas 2019", "28th Birthday, Christmas 2019", "Other","Create Wishlist"};
        ArrayAdapter aa2 = new ArrayAdapter(this,R.layout.item_spinner,wishlist);
        aa2.setDropDownViewResource(R.layout.item_spinner_dropdown);
        binding.spinnerWishList.setAdapter(aa2);
        binding.spinnerWishList.setOnItemSelectedListener(listener);


        binding.etItemName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                binding.tvTextCount.setText(s.length()+"/120");
            }
        });


        binding.etDes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                binding.tvTextDesCount.setText(s.length()+"/120");
            }
        });

        binding.etDes.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                if (binding.etDes.hasFocus()) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK){
                        case MotionEvent.ACTION_SCROLL:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            return true;
                    }
                }
                return false;
            }
        });


        binding.tvRetake.setOnClickListener(v -> {
            callbackImage.openImageChooser((imageFilePath, imageUri, cameraPhotoOrientation) -> {
                this.imageUri = imageUri;
                startCrop(imageUri);

            },true,false);
        });

    }

    private void startCrop(@NonNull Uri uri)
    {
        String destinationFileName = "SampleCropImage";
        destinationFileName += ".jpg";
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), destinationFileName)));
        uCrop = basisConfig(uCrop);
        uCrop = advancedConfig(uCrop);

        uCrop.start(activity);

    }

    private UCrop basisConfig(@NonNull UCrop uCrop) {

        uCrop = uCrop.useSourceImageAspectRatio();
        //uCrop = uCrop.withAspectRatio(1, 1);//radio_square
        return uCrop;
    }

    private UCrop advancedConfig(@NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();

        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);

        options.setCompressionQuality(90);

        options.setHideBottomControls(false);
        options.setFreeStyleCropEnabled(false);

        return uCrop.withOptions(options);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_OK)
        {
            final Uri resultUri = UCrop.getOutput(data);//retake image
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            switch (parent.getId())
            {
                case R.id.spinnerCategory:
                    if(isFirstCategory)
                    {isFirstCategory = !isFirstCategory;
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColorHint)); }
                    else
                    {//((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.colorBlack));
                    }
                    break;

                case R.id.spinnerSize:
                    if(isFirstSize)
                    {
                        isFirstSize = !isFirstSize;
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColorHint));
                    }
                    else
                    {//((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.colorBlack));
                    }
                    break;

                case R.id.spinnerWishList:
                    if(isFirstWishlist)
                    {
                        isFirstWishlist = !isFirstWishlist;
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.textColorHint));
                    }
                    if(wishlist[position].equalsIgnoreCase("Create Wishlist"))
                    {
                        Intent intent = new Intent(activity, CreateWishListActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                        isFirstWishlist = !isFirstWishlist;
                        binding.spinnerWishList.setSelection(0);
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        application.setCurrentActivity(activity);
    }
}
