package com.app.mylineup.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.mylineup.R;
import com.app.mylineup.activity.CreateLittleOneActivirt;
import com.app.mylineup.activity.CreateWishListActivity;
import com.app.mylineup.activity.WishlistActivity;
import com.app.mylineup.adapter.CustomSpinner;
import com.app.mylineup.databinding.FragmentWishlistOneBinding;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.other.Constant;
import com.app.mylineup.other.Global;
import com.app.mylineup.other.Utility;
import com.app.mylineup.pojo.connections.Connection;
import com.app.mylineup.pojo.littleOnes.LittleOne;
import com.app.mylineup.pojo.littleOnes.LittleOnes;
import com.app.mylineup.web_services.PrettyPrinter;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateWishlistFragOne extends BaseFragment
{
    private FragmentWishlistOneBinding binding;
    private String wishlistType = "public";
    private String madeForID = "";
    private List<LittleOne> littleOnes = new ArrayList<>();
    private CustomSpinner customSpinner;
    private String eventDate = "";
    private String wishlistId = null;
    private List<Connection> connectionList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_wishlist_one,container,false);
        init();
        return binding.getRoot();
    }

    private void init()
    {
        customSpinner = new CustomSpinner(getActivity(),R.layout.item_spinner,littleOnes,binding.spinnerMadeFor);
        binding.spinnerMadeFor.setAdapter(customSpinner);
        binding.spinnerMadeFor.setOnItemSelectedListener(listener);

        binding.etWishListName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                binding.tvTextCount.setText(s.length()+"/120");
            }
        });

        binding.tvDate.setOnClickListener(v -> {
            Global.openDatePicker(getActivity(), true, true, new Global.CallbackDateSelect() {
                @Override
                public void OnDateSelect(Calendar myCalendar) {

                    SimpleDateFormat sdf = new SimpleDateFormat(Utility.datePattern);
                    eventDate = sdf.format(myCalendar.getTime());
                    Log.e("TAG","eventDate = "+eventDate);

                    sdf = new SimpleDateFormat(Utility.dayDate);
                    int day = Integer.parseInt(sdf.format(myCalendar.getTime()));
                    sdf = new SimpleDateFormat("MMMM dd, yyyy");
                    String p = sdf.format(myCalendar.getTime());
                    int position = p.indexOf(',');
                    String date = p.substring(0,position)+Utility.getDayOfMonthSuffix(day)+p.substring(position,p.length());
                    binding.tvDate.setText(date);
                }
            });
        });


        binding.llPublic.setOnClickListener(v -> {

            wishlistType = "public";

            binding.tvPublic.setTextColor(getResources().getColor(R.color.lightPink));
            binding.ivPublic.setImageDrawable(getResources().getDrawable(R.drawable.ic_public_selected));
            binding.llPublic.setBackgroundResource(R.drawable.bg_public_private_selected);

            binding.tvPrivate.setTextColor(getResources().getColor(R.color.textColor));
            binding.ivPrivate.setImageDrawable(getResources().getDrawable(R.drawable.ic_private_icon));
            binding.llPrivate.setBackgroundResource(0);

            LinearLayout tabStrip = ((LinearLayout)CreateWishListActivity.binding.tabLayout.getChildAt(0));
            for(int i = 0; i < tabStrip.getChildCount(); i++) {
                tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return true;
                    }
                });
            }
        });

        binding.llPrivate.setOnClickListener(v -> {

            wishlistType = "private";

            binding.tvPrivate.setTextColor(getResources().getColor(R.color.lightPink));
            binding.ivPrivate.setImageDrawable(getResources().getDrawable(R.drawable.ic_private_icon_selected));
            binding.llPrivate.setBackgroundResource(R.drawable.bg_public_private_selected);

            binding.tvPublic.setTextColor(getResources().getColor(R.color.textColor));
            binding.ivPublic.setImageDrawable(getResources().getDrawable(R.drawable.ic_public));
            binding.llPublic.setBackgroundResource(0);

            LinearLayout tabStrip = ((LinearLayout)CreateWishListActivity.binding.tabLayout.getChildAt(0));
            for(int i = 0; i < tabStrip.getChildCount(); i++) {
                tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return false;
                    }
                });
            }

        });

        binding.tvCreateWishlist.setOnClickListener(view -> {
            if(!Global.isValidString(binding.etWishListName.getText().toString()))
            {dialogCommon.showDialog(getString(R.string.please_enter_wishlist_name));}
            else if(madeForID.equalsIgnoreCase(""))
            {dialogCommon.showDialog(getString(R.string.please_select_wishlist_made_for));}
            else if(!Global.isValidString(binding.tvDate.getText().toString()))
            {dialogCommon.showDialog(getString(R.string.please_select_occasion_date));}
            else
            {
                callCreateWishlistApi();
            }

        });

        callLittleOneApi();
    }

    public void callCreateWishlistApi()
    {
        loaderDialog.show();

        String privateListId = null;
        if(wishlistType.equalsIgnoreCase("private") && connectionList != null)
        {
            for(int  i = 0;i<connectionList.size();i++)
            {
                if(connectionList.get(i).isSelected())
                {
                    if(privateListId == null)
                    {
                        privateListId =  connectionList.get(i).getId();
                    }
                    else
                    {
                        privateListId = privateListId+","+connectionList.get(i).getId();
                    }

                }
            }
        }
        Log.e("TAG","privateListId = "+privateListId);
        /*if(privateListId == null)
        {privateListId = loginData.getUser().getId();}*/


        Call<Object> call = application.getApis().addEditWishlist(madeForID,binding.etWishListName.getText().toString().trim(),
                loginData.getUser().getId().equalsIgnoreCase(madeForID) ? "1":"2",eventDate, wishlistType.equalsIgnoreCase("private")?"1":"0",wishlistId,privateListId);
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG","response = "+x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(),dialogCommon,context,application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if(jsonObject.has("status") && jsonObject.getBoolean("status"))
                    {
                        binding.etWishListName.setText("");
                        eventDate = "";
                        binding.tvDate.setText("");
                        dialogCommon.showDialog(jsonObject.getString("message"), new AlertDialogCommon.CallBackClickListener() {
                            @Override
                            public void OnDialogPositiveBtn() {
                                Intent intent = new Intent(getActivity(), WishlistActivity.class);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
                            }

                            @Override
                            public void OnDialogNegativeBtn() {

                            }
                        });
                    }
                    else
                    {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                }
                catch (Exception e)
                {}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    private void callLittleOneApi()
    {
        loaderDialog.show();
        Call<Object> call = application.getApis().littleOneList(loginData.getUser().getId());
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG","response = "+x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(),dialogCommon,context,application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if(jsonObject.has("status") && jsonObject.getBoolean("status"))
                    {
                        LittleOnes littleOne = gson.fromJson(x,LittleOnes.class);

                        LittleOne littleOne1 = new LittleOne();
                        littleOne1.setFirstName(loginData.getUser().getFirstName());
                        littleOne1.setLastName(loginData.getUser().getLastName()+" (Me)");
                        littleOne1.setId(loginData.getUser().getId());
                        littleOnes.add(littleOne1);

                        littleOnes.addAll(littleOne.getLittleOne());

                        LittleOne littleOne2 = new LittleOne();
                        littleOne2.setFirstName("");
                        littleOne2.setLastName("");
                        littleOne2.setId("");
                        littleOnes.add(littleOne2);

                        customSpinner.notifyDataSetChanged();
                    }
                    else
                    {
                        LittleOne littleOne1 = new LittleOne();
                        littleOne1.setFirstName(loginData.getUser().getFirstName());
                        littleOne1.setLastName(loginData.getUser().getLastName()+" (Me)");
                        littleOne1.setId(loginData.getUser().getId());
                        littleOnes.add(littleOne1);

                        LittleOne littleOne2 = new LittleOne();
                        littleOne2.setFirstName("");
                        littleOne2.setLastName("");
                        littleOne2.setId("");
                        littleOnes.add(littleOne2);

                        customSpinner.notifyDataSetChanged();
                        //dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                }
                catch (Exception e)
                {}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    private AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            Log.e("TAG","little one = "+littleOnes.get(position));
            madeForID = littleOnes.get(position).getId();

            if((littleOnes.size()-1) == position)
            {
                binding.spinnerMadeFor.setSelection(0);
                Intent intent = new Intent(context, CreateLittleOneActivirt.class);
                context.startActivity(intent);
                ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    public void setList(List<Connection> connectionList)
    {
        this.connectionList = connectionList;
    }
}
