package com.app.mylineup.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alphabetik.Alphabetik;
import com.app.mylineup.R;
import com.app.mylineup.activity.AccountSettingActivity;
import com.app.mylineup.activity.ContactListActivity;
import com.app.mylineup.adapter.ConnectionAdapter;
import com.app.mylineup.databinding.FragmentConnectionBinding;
import com.app.mylineup.pojo.connections.Connection;
import com.app.mylineup.pojo.connections.Connections;
import com.app.mylineup.web_services.PrettyPrinter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConnectionFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener
{
    private FragmentConnectionBinding binding;
    private ConnectionAdapter adapter;

    private Connections connections;
    private List<Connection> connectionList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_connection,container,false);
        init();
        return binding.getRoot();
    }

    private void init()
    {
        String[] customAlphabet = getResources().getStringArray(R.array.my_string_abcd);
        binding.alphSectionIndex.setAlphabet(customAlphabet);

        binding.alphSectionIndex.onSectionIndexClickListener(new Alphabetik.SectionIndexClickListener() {
            @Override
            public void onItemClick(View view, int position, String character) {
                String info = " Position = " + position + " Char = " + character;
                Log.e("View: ", view + "," + info);
                binding.rvConnection.smoothScrollToPosition(getPositionFromData(character));
            }
        });

        binding.swipeRefresh.setOnRefreshListener(this);

        binding.rvConnection.setLayoutManager(new LinearLayoutManager(context));
        adapter = new ConnectionAdapter(context,connectionList);
        binding.rvConnection.setAdapter(adapter);

        binding.ivFindFriend.setOnClickListener(v -> {
            Intent intent = new Intent(context, ContactListActivity.class);
            startActivity(intent);
            ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        });

        callMyConnectionApi();

    }

    private void callMyConnectionApi()
    {
        loaderDialog.show();
        Call<Object> call = application.getApis().myConnections(loginData.getUser().getId());
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG","response = "+x);
                loaderDialog.dismiss();
                if(binding.swipeRefresh.isRefreshing())
                {binding.swipeRefresh.setRefreshing(false);}
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if(jsonObject.has("status") && jsonObject.getBoolean("status"))
                    {
                        connections = gson.fromJson(x,Connections.class);
                        if(connections.getConnections() != null)
                        {
                            connectionList.clear();
                            connectionList.addAll(connections.getConnections());
                            adapter.notifyDataSetChanged();
                        }
                    }
                }
                catch (Exception e)
                {}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }

    private int getPositionFromData(String character) {
        for(int i = 0;i<connectionList.size();i++)
        {
            if((connectionList.get(i).getFirstName().charAt(0)+"").equalsIgnoreCase("" + character))
            {
                return i;
            }
        }
        return 0;
    }

    @Override
    public void onRefresh()
    {
        callMyConnectionApi();
    }
}
