package com.app.mylineup.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.mylineup.R;
import com.app.mylineup.adapter.GiftAdapter;
import com.app.mylineup.adapter.UpComingOccasionAdapter;
import com.app.mylineup.databinding.FragmentGiftfeedBinding;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

public class GiftFeedFragment extends BaseFragment
{
    private FragmentGiftfeedBinding binding;
    private UpComingOccasionAdapter upComingOccasionAdapter;
    private GiftAdapter giftAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_giftfeed,container,false);
        init();
        return binding.getRoot();
    }

    private void init()
    {
        binding.rvUpcomingOccasion.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));
        upComingOccasionAdapter = new UpComingOccasionAdapter(context);
        binding.rvUpcomingOccasion.setAdapter(upComingOccasionAdapter);
        binding.rvUpcomingOccasion.setNestedScrollingEnabled(false);

        binding.rvGiftFeed.setLayoutManager(new LinearLayoutManager(context));
        giftAdapter = new GiftAdapter(context);
        binding.rvGiftFeed.setAdapter(giftAdapter);
        binding.rvGiftFeed.setNestedScrollingEnabled(false);
    }
}
