package com.app.mylineup.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.mylineup.R;
import com.app.mylineup.activity.CreateWishListActivity;
import com.app.mylineup.adapter.UserWishlistAdapter;
import com.app.mylineup.databinding.FragmentUserWishlistBinding;
import com.app.mylineup.other.Constant;
import com.app.mylineup.pojo.WishlistBeen;
import com.app.mylineup.pojo.WishlistImageBeen;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class UserWishlistFragment extends BaseFragment {

    private FragmentUserWishlistBinding binding;

    private UserWishlistAdapter wishlistAdapter;
    private List<WishlistBeen> wishlistBeenList = new ArrayList<>();
    private List<WishlistImageBeen> wishlistImageBeenList = new ArrayList<>();
    private boolean isMyAcc;

    public UserWishlistFragment()
    {}

    @SuppressLint("ValidFragment")
    public UserWishlistFragment(boolean isMyAcc)
    {
        this.isMyAcc = isMyAcc;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_user_wishlist, container, false);
        init();
        return binding.getRoot();
    }

    private void init()
    {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.rvUserWishlist.setLayoutManager(layoutManager);
        wishlistAdapter = new UserWishlistAdapter(getActivity(), wishlistBeenList,isMyAcc);
        binding.rvUserWishlist.setNestedScrollingEnabled(false);

        if(isMyAcc)
        {
            binding.llNewWishList.setVisibility(View.VISIBLE);
            binding.llNewWishList.setOnClickListener(view -> {
                Intent intent = new Intent(getActivity(), CreateWishListActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);

            });
        }

        callWishlistImageApi();

    }

    private void callWishlistImageApi()
    {
        wishlistImageBeenList.add(new WishlistImageBeen(R.drawable.ic_dummy));
        wishlistImageBeenList.add(new WishlistImageBeen(R.drawable.ic_dummy));
        wishlistImageBeenList.add(new WishlistImageBeen(R.drawable.ic_dummy));
        wishlistImageBeenList.add(new WishlistImageBeen(R.drawable.ic_dummy));

        callWishlistApi();
    }

    private void callWishlistApi()
    {

        wishlistBeenList.add(new WishlistBeen("","","","","", wishlistImageBeenList));
        wishlistBeenList.add(new WishlistBeen("","","","","", wishlistImageBeenList));
        wishlistBeenList.add(new WishlistBeen("","","","","", wishlistImageBeenList));
        wishlistBeenList.add(new WishlistBeen("","","","","", wishlistImageBeenList));
        wishlistBeenList.add(new WishlistBeen("","","","","", wishlistImageBeenList));
        wishlistBeenList.add(new WishlistBeen("","","","","", wishlistImageBeenList));

        binding.rvUserWishlist.setAdapter(wishlistAdapter);
    }
}
