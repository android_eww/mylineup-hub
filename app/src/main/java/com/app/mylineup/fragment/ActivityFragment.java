package com.app.mylineup.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.mylineup.R;
import com.app.mylineup.adapter.ActivityAdapter;
import com.app.mylineup.databinding.FragmentActivityBinding;
import com.app.mylineup.pojo.WishlistBeen;
import com.app.mylineup.pojo.WishlistImageBeen;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ActivityFragment extends BaseFragment {

    private String TAG = "Tab2Fragment";
    private FragmentActivityBinding binding;

    private ActivityAdapter wishlistAdapter;
    private List<WishlistBeen> wishlistBeenList = new ArrayList<>();
    private List<WishlistImageBeen> wishlistImageBeenList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_activity,container,false);
        init();
        return binding.getRoot();
    }

    private void init()
    {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.rvActivity.setLayoutManager(layoutManager);
        wishlistAdapter = new ActivityAdapter(getActivity(), wishlistBeenList);
        binding.rvActivity.setNestedScrollingEnabled(false);

        callWishlistImageApi();
    }

    private void callWishlistImageApi()
    {
        Log.e(TAG,"callWishlistImageApi()");

        wishlistImageBeenList.clear();

        wishlistImageBeenList.add(new WishlistImageBeen(R.drawable.ic_dummy));
        wishlistImageBeenList.add(new WishlistImageBeen(R.drawable.ic_dummy));
        wishlistImageBeenList.add(new WishlistImageBeen(R.drawable.ic_dummy));
        wishlistImageBeenList.add(new WishlistImageBeen(R.drawable.ic_dummy));

        callWishlistApi();
    }

    private void callWishlistApi()
    {
        Log.e(TAG,"callWishlistApi()");

        wishlistBeenList.clear();

        wishlistBeenList.add(new WishlistBeen("","","","","", wishlistImageBeenList));
        wishlistBeenList.add(new WishlistBeen("","","","","", wishlistImageBeenList));
        wishlistBeenList.add(new WishlistBeen("","","","","", wishlistImageBeenList));

        binding.rvActivity.setAdapter(wishlistAdapter);
    }
}
