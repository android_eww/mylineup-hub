package com.app.mylineup.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import com.app.mylineup.R;
import com.app.mylineup.adapter.WishListAdapter;
import com.app.mylineup.databinding.FragmentItemListBinding;
import com.app.mylineup.other.Constant;

public class WishlistItemsFragment extends BaseFragment
{
    private FragmentItemListBinding binding;
    private WishListAdapter wishListAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_item_list,container,false);
        init();
        return binding.getRoot();
    }

    private void init()
    {
        binding.llTopNewWishList.setVisibility(View.GONE);

        wishListAdapter = new WishListAdapter(context, Constant.FROM_WISHLIST);
        binding.rvWishList.setLayoutManager(new GridLayoutManager(getActivity(),2));
        binding.rvWishList.setAdapter(wishListAdapter);
    }
}
