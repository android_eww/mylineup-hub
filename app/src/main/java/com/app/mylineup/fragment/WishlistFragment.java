package com.app.mylineup.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.mylineup.R;
import com.app.mylineup.activity.AccountSettingActivity;
import com.app.mylineup.activity.LoginActivity;
import com.app.mylineup.activity.UserProfileActivity;
import com.app.mylineup.adapter.WishListAdapter;
import com.app.mylineup.databinding.FragmentWishlistBinding;
import com.app.mylineup.dialog.AlertListDialog;
import com.app.mylineup.other.Constant;
import com.app.mylineup.pojo.CommonPojo;
import com.app.mylineup.pojo.OnlyString;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;

public class WishlistFragment extends BaseFragment
{
    private FragmentWishlistBinding binding;

    private WishlistListFragment wishlistListFragment;
    private WishlistItemsFragment itemsFragment;
    private FragmentTransaction transaction;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_wishlist,container,false);
        init();
        return binding.getRoot();
    }

    private void init()
    {
        wishlistListFragment = new WishlistListFragment();
        itemsFragment = new WishlistItemsFragment();

        loadListFragment();

        binding.ivAccSetting.setOnClickListener(v -> {
            Intent intent = new Intent(context, UserProfileActivity.class);
            intent.putExtra(Constant.FROM,Constant.FROM_MYPROFILE);
            startActivity(intent);
            ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        });

        binding.tvLists.setOnClickListener(v -> {
            binding.tvLists.setTextColor(context.getResources().getColor(R.color.colorAccent));
            binding.tvItems.setTextColor(context.getResources().getColor(R.color.textColor));

            binding.tvLists.setBackground(context.getResources().getDrawable(R.drawable.bg_wishlist_selected));
            binding.tvItems.setBackground(null);

            loadListFragment();
        });

        binding.tvItems.setOnClickListener(v -> {
            binding.tvLists.setTextColor(context.getResources().getColor(R.color.textColor));
            binding.tvItems.setTextColor(context.getResources().getColor(R.color.colorAccent));

            binding.tvLists.setBackground(null);
            binding.tvItems.setBackground(context.getResources().getDrawable(R.drawable.bg_wishlist_selected));

            loadItemsFragment();
        });

        binding.tvOccasion.setOnClickListener(v -> {

            ArrayList<CommonPojo> aboutMeBeen = new ArrayList<>();
            aboutMeBeen.add(new OnlyString("All Occasions"));
            aboutMeBeen.add(new OnlyString("Christmas"));
            aboutMeBeen.add(new OnlyString("Birthday"));

            listDialog.showDialog("",true,getString(R.string.label_cancel),aboutMeBeen, new AlertListDialog.CallBackClickListener() {
                @Override
                public void OnItemSelected(CommonPojo commonPojo, int position) {
                    if(commonPojo instanceof OnlyString)
                    {
                        OnlyString been = (OnlyString) commonPojo;
                        Log.e("TAG","item = "+been.getTitle());
                        binding.tvOccasion.setText(been.getTitle());
                    }
                }
            });

        });
    }

    private void loadItemsFragment()
    {
        transaction = getChildFragmentManager().beginTransaction();
        if(wishlistListFragment.isAdded())
        {transaction.hide(wishlistListFragment);}
        if(itemsFragment.isAdded())
        {transaction.show(itemsFragment);}
        else
        {transaction.add(R.id.frameLayout, itemsFragment);}

        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commitAllowingStateLoss();
    }

    private void loadListFragment()
    {
        transaction = getChildFragmentManager().beginTransaction();
        if(wishlistListFragment.isAdded())
        {transaction.show(wishlistListFragment);}
        else
        {transaction.add(R.id.frameLayout, wishlistListFragment);}
        if(itemsFragment.isAdded())
        {transaction.hide(itemsFragment);}

        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commitAllowingStateLoss();
    }
}
