package com.app.mylineup.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.mylineup.application.Application;
import com.app.mylineup.dialog.AlertDialogCommon;
import com.app.mylineup.dialog.AlertListDialog;
import com.app.mylineup.dialog.LoaderDialog;
import com.app.mylineup.other.Constant;
import com.app.mylineup.pojo.loginData.LoginData;
import com.google.gson.Gson;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class BaseFragment extends Fragment
{
    protected Context context;

    protected AlertDialogCommon dialogCommon;
    protected AlertListDialog listDialog;
    protected LoaderDialog loaderDialog;

    protected Application application;

    protected Gson gson;
    protected LoginData loginData;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

        gson = new Gson();
        dialogCommon = new AlertDialogCommon(context);
        loaderDialog = new LoaderDialog(context);
        listDialog = new AlertListDialog(context);

        application = Application.getInstance();
        loginData = application.getSharedPref().getUserSession(Constant.LOGIN_DATA);
    }
}
