package com.app.mylineup.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.mylineup.R;
import com.app.mylineup.adapter.AboutAdapter;
import com.app.mylineup.adapter.LikeInterestAdapter;
import com.app.mylineup.databinding.FragmentAboutMeBinding;
import com.app.mylineup.pojo.AboutMeBeen;
import com.google.android.flexbox.AlignItems;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import fisk.chipcloud.ChipCloud;
import fisk.chipcloud.ChipCloudConfig;

public class AboutMeFragment extends BaseFragment {

    private FragmentAboutMeBinding binding;
    private String TAG = "Tab3Fragment";
    private AboutAdapter aboutAdapter;
    private List<AboutMeBeen> aboutMeBeenList = new ArrayList<>();

    private LikeInterestAdapter interestAdapter;
    private List<AboutMeBeen> interestList = new ArrayList<>();

    ChipCloudConfig config;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_about_me,container,false);
        init();
        return binding.getRoot();
    }

    private void init()
    {
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(),2);
        binding.rvPreference.setLayoutManager(layoutManager);
        aboutAdapter = new AboutAdapter(getActivity(), aboutMeBeenList);
        binding.rvPreference.setAdapter(aboutAdapter);
        binding.rvPreference.setNestedScrollingEnabled(false);

        interestAdapter = new LikeInterestAdapter(getActivity(),interestList);
        binding.rvLikeInterest.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvLikeInterest.setNestedScrollingEnabled(false);
        binding.rvLikeInterest.setAdapter(interestAdapter);

        config = new ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedTextColor(getResources().getColor(R.color.colorBlack))
                .uncheckedTextColor(getResources().getColor(R.color.colorBlack))
                .chipTextSize(16f)
                .chipPadding(20, 10, 20, 10);
        callAboutMeApi();
        setTopBrandFlex();
        setFavoriteStoreFlex();
    }

    private void setFavoriteStoreFlex()
    {
        binding.flexBoxStore.setAlignItems(AlignItems.CENTER);

        final ChipCloud chipCloud = new ChipCloud(getActivity(), binding.flexBoxStore, config);

        chipCloud.addChip("Dick’s Sporting Goods", null);
        chipCloud.addChip("Macy’s", null);
        chipCloud.addChip("PACSUN", null);
        chipCloud.addChip("Nordstrom Rack", null);
        chipCloud.addChip("ULAH", null);
        chipCloud.addChip("Made in KC", null);
        chipCloud.addChip("Nordstrom", null);
        chipCloud.addChip("Best Buy", null);
        chipCloud.addChip("Cabella’s", null);
    }

    private void setTopBrandFlex()
    {
        binding.flexBoxBrand.setAlignItems(AlignItems.CENTER);

        final ChipCloud chipCloud = new ChipCloud(getActivity(), binding.flexBoxBrand, config);

        chipCloud.addChip("Nike", null);
        chipCloud.addChip("Remington", null);
        chipCloud.addChip("EASE label", null);
        chipCloud.addChip("Adidas", null);
        chipCloud.addChip("Mitchell & Ness", null);
        chipCloud.addChip("A.C.W.", null);
        chipCloud.addChip("5.11 Tactical", null);
        chipCloud.addChip("Supreme", null);
        chipCloud.addChip("KITH", null);
        chipCloud.addChip("ORO Los Angeles", null);
    }

    private void callAboutMeApi()
    {
        Log.e(TAG,"callAboutMe()");

        aboutMeBeenList.clear();

        aboutMeBeenList.add(new AboutMeBeen("","SHOES","12.5",R.drawable.ic_shoes_icon));
        aboutMeBeenList.add(new AboutMeBeen("","SIZING PREFERENCE","Men's",R.drawable.ic_sizing_icon));
        aboutMeBeenList.add(new AboutMeBeen("","SHIRTS","Medium",R.drawable.ic_shirts_icon));
        aboutMeBeenList.add(new AboutMeBeen("","INSEAM/WAIST","32/34",R.drawable.ic_inseam_waist_icon));
        aboutMeBeenList.add(new AboutMeBeen("","COLLAR/SLEEVE","16/34.5",R.drawable.ic_collar_sleeve_icon));

        interestList.add(new AboutMeBeen("","FAVORITE BEVERAGE","Coke, WhiteClaw",R.drawable.ic_beverages_icon));
        interestList.add(new AboutMeBeen("","BOOK GENRES","Non-Fiction",R.drawable.ic_book_icon));
        interestList.add(new AboutMeBeen("","FOOD TYPES","Mexican, italian",R.drawable.ic_food_icon));
        interestList.add(new AboutMeBeen("","HOBBIES/ACTIVITIES","Hiking, Travel, Concerts",R.drawable.ic_hobbies_icon));
        interestList.add(new AboutMeBeen("","IDOLS","Patrick Mahomes, Keanu Reeves",R.drawable.ic_idols_icon));
        interestList.add(new AboutMeBeen("","MUSIC GENRES","Country, Hip Hop, Alternative",R.drawable.ic_music_icon));
        interestList.add(new AboutMeBeen("","RESTAURANTS","Q39, Chuy’s, Garozzo’s, Joe’s Kansas City",R.drawable.ic_restaurants_icon));

        interestAdapter.notifyDataSetChanged();
        aboutAdapter.notifyDataSetChanged();
    }
}
