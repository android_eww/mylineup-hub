package com.app.mylineup.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.mylineup.R;
import com.app.mylineup.activity.AccountSettingActivity;
import com.app.mylineup.adapter.NewWishListAdapter;
import com.app.mylineup.adapter.RequestAdapter;
import com.app.mylineup.databinding.FragmentNotificationBinding;
import com.app.mylineup.web_services.PrettyPrinter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationFragment extends BaseFragment implements RequestAdapter.FriendRequstAction
{
    private FragmentNotificationBinding binding;
    private RequestAdapter requestAdapter;
    private NewWishListAdapter newWishListAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification,container,false);
        init();
        return binding.getRoot();
    }

    private void init()
    {
        binding.rvRequest.setLayoutManager(new LinearLayoutManager(context));
        requestAdapter = new RequestAdapter(context,this);
        binding.rvRequest.setAdapter(requestAdapter);

        binding.rvNewWishList.setLayoutManager(new LinearLayoutManager(context));
        newWishListAdapter = new NewWishListAdapter(context);
        binding.rvNewWishList.setAdapter(newWishListAdapter);

        binding.ivSetting.setOnClickListener(v -> {
            Intent intent = new Intent(context, AccountSettingActivity.class);
            startActivity(intent);
            ((Activity)context).overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);
        });
    }

    @Override
    public void perFormActionOnRequest(String requestType)
    {
        Call<Object> call = application.getApis().friendRequest(loginData.getUser().getId(),"",requestType);
        Log.e("TAG","url = "+call.request().url());
        Log.e("TAG","param = "+ PrettyPrinter.print(call));

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                String x = gson.toJson(response.body());
                Log.e("TAG","response = "+x);
                loaderDialog.dismiss();
                PrettyPrinter.checkStatusCode(response.code(),dialogCommon,context,application);
                try {
                    JSONObject jsonObject = new JSONObject(x);
                    if(jsonObject.has("status") && jsonObject.getBoolean("status"))
                    {
                    }
                    else
                    {
                        dialogCommon.showDialog(jsonObject.getString("message"));
                    }
                }
                catch (Exception e)
                {}
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                loaderDialog.dismiss();
            }
        });
    }
}
