package com.app.mylineup.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mylineup.R;
import com.app.mylineup.activity.CreateWishListActivity;
import com.app.mylineup.adapter.UserWishlistAdapter;
import com.app.mylineup.databinding.FragmentItemListBinding;
import com.app.mylineup.pojo.WishlistBeen;
import com.app.mylineup.pojo.WishlistImageBeen;

import java.util.ArrayList;
import java.util.List;

public class WishlistListFragment extends BaseFragment
{
    private FragmentItemListBinding binding;
    private UserWishlistAdapter wishlistAdapter;
    private List<WishlistBeen> wishlistBeenList = new ArrayList<>();
    private List<WishlistImageBeen> wishlistImageBeenList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_item_list,container,false);
        init();
        return binding.getRoot();
    }

    private void init()
    {
        binding.llTopNewWishList.setVisibility(View.VISIBLE);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.rvWishList.setLayoutManager(layoutManager);
        wishlistAdapter = new UserWishlistAdapter(getActivity(), wishlistBeenList,true);
        binding.rvWishList.setNestedScrollingEnabled(false);

        binding.rvWishList.setPadding(0,0,0,0);

        wishlistImageBeenList.add(new WishlistImageBeen(R.drawable.ic_dummy));
        wishlistImageBeenList.add(new WishlistImageBeen(R.drawable.ic_dummy));
        wishlistImageBeenList.add(new WishlistImageBeen(R.drawable.ic_dummy));
        wishlistImageBeenList.add(new WishlistImageBeen(R.drawable.ic_dummy));

        wishlistBeenList.add(new WishlistBeen("","","","","", wishlistImageBeenList));
        wishlistBeenList.add(new WishlistBeen("","","","","", wishlistImageBeenList));
        wishlistBeenList.add(new WishlistBeen("","","","","", wishlistImageBeenList));
        wishlistBeenList.add(new WishlistBeen("","","","","", wishlistImageBeenList));
        wishlistBeenList.add(new WishlistBeen("","","","","", wishlistImageBeenList));
        wishlistBeenList.add(new WishlistBeen("","","","","", wishlistImageBeenList));

        binding.rvWishList.setAdapter(wishlistAdapter);

        binding.llNewWishList.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), CreateWishListActivity.class);
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.enter_from_right,R.anim.exit_to_left);

        });
    }
}
